/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #3 Exercise #2

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <stdint.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdbool.h> 
#include <string.h> 
#include <math.h> 
#include <avr/io.h> 
#include <avr/interrupt.h> 
#include <avr/eeprom.h> 
#include <avr/portpins.h> 
#include <avr/pgmspace.h> 
 
//FreeRTOS include files 
#include "FreeRTOS.h" 
#include "task.h" 
#include "croutine.h" 
enum LEDState {led_off, led_on} PD0_state, PD2_state, PD4_state;

// void LEDS_Init(){
// 	PD0_state = led_off;
// 	PD2_state = led_off;
// 	PD4
// }

void PD0_Tick(){
	//Actions
	switch(PD0_state){
		case led_off:
			PORTD = PORTD ^ (0x01);
			break;
		case led_on:
			PORTD = PORTD | (0x01);
			break;
		default:
			PORTD = PORTD ^ (0x01);
		break;
	}
	//Transitions
	switch(PD0_state){
		case led_off:
			PD0_state = led_on;
			break;
		case led_on:
			PD0_state = led_off;
			break;
		default:
			PD0_state = led_off;
			break;
	}
}

void PD2_Tick(){
	//Actions
	switch(PD2_state){
		case led_off:
			PORTD = PORTD ^ (0x04);
			break;
		case led_on:
			PORTD = PORTD | (0x04);
			break;
		default:
			PORTD = PORTD ^ (0x04);
			break;
	}
	//Transitions
	switch(PD2_state){
		case led_off:
		PD2_state = led_on;
		break;
		case led_on:
		PD2_state = led_off;
		break;
		default:
		PD2_state = led_off;
		break;
	}
}

void PD4_Tick(){
	//Actions
	switch(PD4_state){
		case led_off:
			PORTD = PORTD ^ (0x10);
			break;
		case led_on:
			PORTD = PORTD | (0x10);
			break;	
		default:
			PORTD = PORTD ^ (0x010);
			break;
	}
	//Transitions
	switch(PD4_state){
		case led_off:
		PD4_state = led_on;
		break;
		case led_on:
		PD4_state = led_off;
		break;
		default:
		PD4_state = led_off;
		break;
	}
}

void PD0SecTask()
{
	PD0_state = led_off;
   for(;;) 
   { 	
	PD0_Tick();
	vTaskDelay(500);
   } 
}

void PD2SecTask()
{
	PD2_state = led_off;
	for(;;)
	{
		PD2_Tick();
		vTaskDelay(1000);
	}
}

void PD4SecTask()
{
	PD4_state = led_off;
	for(;;)
	{
		PD4_Tick();
		vTaskDelay(2500);
	}
}

void StartPD0SecPulse(unsigned portBASE_TYPE Priority)
{
	xTaskCreate(PD0SecTask, (signed portCHAR *)"PD0SecTask", configMINIMAL_STACK_SIZE, NULL, Priority, NULL );
}	



void StartPD2SecPulse(unsigned portBASE_TYPE Priority)
{
	xTaskCreate(PD2SecTask, (signed portCHAR *)"PD2SecTask", configMINIMAL_STACK_SIZE, NULL, Priority, NULL );
}


void StartPD4SecPulse(unsigned portBASE_TYPE Priority)
{
	xTaskCreate(PD4SecTask, (signed portCHAR *)"PD4SecTask", configMINIMAL_STACK_SIZE, NULL, Priority, NULL );
}
 
int main(void) 
{ 
   DDRA = 0x00;
   DDRD = 0xFF;
   //Start Tasks  
   StartPD0SecPulse(1);
   StartPD2SecPulse(1);
   StartPD4SecPulse(1);
    //RunSchedular 
   vTaskStartScheduler(); 
 
   return 0; 
}