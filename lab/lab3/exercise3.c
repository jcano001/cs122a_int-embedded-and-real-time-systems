/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #2 Exercise #3

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/portpins.h>
#include <avr/pgmspace.h>

//FreeRTOS include files
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
enum LEDState {shift_left, shift_right} led_state;

void LEDS_Init(){
	PORTD = 0x01;
	led_state = shift_left;
}

void LEDS_Tick(){
	//Actions
	switch(led_state){
		case shift_left:
			PORTD = PORTD << 1;
			break;
		case shift_right:
			PORTD = PORTD >> 1;
			break;
		default:
			PORTD = 0;
			break;
	}
	//Transitions
	switch(led_state){
		case shift_left:
			if(PORTD != 0x80)
				led_state = shift_left;
			else
				led_state = shift_right;
			break;
		case shift_right:
			if(PORTD != 0x01)
				led_state = shift_right;
			else
				led_state = shift_left;
			break;
		default:
			led_state = shift_left;
			break;
	}
}

void LedSecTask()
{
	LEDS_Init();
   for(;;) 
   { 	
	LEDS_Tick();
	vTaskDelay(100); 
   } 
}

void StartSecPulse(unsigned portBASE_TYPE Priority)
{
	xTaskCreate(LedSecTask, (signed portCHAR *)"LedSecTask", configMINIMAL_STACK_SIZE, NULL, Priority, NULL );
}	
 
int main(void) 
{ 
   DDRA = 0x00;
   DDRD = 0xFF;
   //Start Tasks  
   StartSecPulse(1);
    //RunSchedular 
   vTaskStartScheduler(); 
 
   return 0; 
}