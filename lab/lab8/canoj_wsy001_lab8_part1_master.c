/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #8 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/


#include <avr/io.h>
#include "bit.h"
#include "keypad.h"
#include "lcd.h"
#include "scheduler.h"

// Master code
void SPI_MasterInit(void) {
        // Set DDRB to have MOSI, SCK, and SS as output and MISO as input
        DDRB = 0xBF; PORTB = 0x40;
        // Set SPCR register to enable SPI, enable master, and use SCK frequency
        SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0);
        //   of fosc/16  (pg. 168)
        // Make sure global interrupts are enabled on SREG register (pg. 9)
        SREG =0x80;
}

void SPI_MasterTransmit(unsigned char cData) {      
        // set SS low
        PORTB = SetBit(PORTB,4,0);
		// data in SPDR will be transmitted, e.g. SPDR = cData;
		SPDR = cData;
        while(!(SPSR & (1<<SPIF))) { // wait for transmission to complete
        ;
        }
        // set SS high
        PORTB = SetBit(PORTB,4,1);      
}

// Servant code
void SPI_ServantInit(void) {
        // set DDRB to have MISO line as output and MOSI, SCK, and SS as input
        DDRB = 0x40; PORTB = 0xBF;
        // set SPCR register to enable SPI and enable SPI interrupt (pg. 168)
        SPCR |= (1<<SPE) | (1<<SPIE);
        // make sure global interrupts are enabled on SREG register (pg. 9)
        SREG = 0x80;
}

unsigned receivedData;
/*ISR(SPI_STC_vect) { // this is enabled in with the SPCR register?s ?SPI
  // Interrupt Enable?
        // SPDR contains the received data, e.g. unsigned char receivedData =
// SPDR;
        receivedData = SPDR;
}*/

int count = 0;
enum masterStates {wait, send};
int TickFct_master( int state ) {
        switch( state ) {
                case -1:
                        state = wait;
                        break;
                case wait:
                        state = send;
                        break;
                case send:
                        state = wait;
                        break;
                default:
                        break;          
        }
        
        switch( state ) {
                case wait:
                        PORTA = SetBit(PORTA,2,0);
						count++;
						if( count >= 0xFF )
							count = 0;
                        break;
                case send:
                        PORTA = SetBit(PORTA,2,1);
                        SPI_MasterTransmit(count);
                        break;
                default:
                        break;
        }
        
        return state;
}

enum servantStates {s_wait, read};
int TickFct_servant( int state ) {
        switch( state )  {
                case -1:
                        state = s_wait;
                        break;
                case s_wait:
                        state = read;
                        break;
                case read:
                        state = s_wait;
                        break;
                default:
                        state = s_wait;
                        break;
        }
        
        switch( state ) {
                case s_wait:
                        PORTA = SetBit(PORTA,0,0);
						break;
                case read:
                        PORTA = SetBit(PORTA,0,1);
                        PORTC = receivedData;
                        break;
                default:
                        break;
        }
        
        return state;
}

int main(void)
{
        // MASTER
        DDRC = 0xF0; PORTC = 0x0F;
        DDRD = 0xFF; PORTD = 0x00;
        DDRA = 0xFF; PORTA = 0x00;
        SPI_MasterInit();
        
        //SLAVE
        /*SPI_ServantInit();
        DDRC = 0xFF; PORTC = 0x00;
        DDRA = 0xFF; PORTA = 0x00;*/
        
        //LCD_init();
                
        tasksNum = 1; // declare number of tasks
        task tsks[1]; // initialize the task array
        tasks = tsks; // set the task array
        
        // define tasks
        unsigned char i=0; // task counter
        tasks[i].state = -1;
        tasks[i].period = 1000;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_master;
        
        TimerSet(100);
        TimerOn();
        
        char c;
    while(1)
    {
        //TODO:: Please write your application code 
                //SetBit(PORTA,2,1);
    }
}