/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #8 Exercise #3

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/


#include <avr/io.h>
#include "bit.h"
#include "keypad.h"
#include "lcd.h"
#include "scheduler.h"

// Master code
void SPI_MasterInit(void) {
        // Set DDRB to have MOSI, SCK, and SS as output and MISO as input
        DDRB = 0xBF; PORTB = 0x40;
        // Set SPCR register to enable SPI, enable master, and use SCK frequency
        SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0);
        //   of fosc/16  (pg. 168)
        // Make sure global interrupts are enabled on SREG register (pg. 9)
        SREG =0x80;
}

void SPI_MasterTransmit(unsigned char cData) {      
        // set SS low
        PORTB = SetBit(PORTB,4,0);
                // data in SPDR will be transmitted, e.g. SPDR = cData;
                SPDR = cData;
        while(!(SPSR & (1<<SPIF))) { // wait for transmission to complete
        ;
        }
        // set SS high
        PORTB = SetBit(PORTB,4,1);      
}

// Servant code
void SPI_ServantInit(void) {
        // set DDRB to have MISO line as output and MOSI, SCK, and SS as input
        DDRB = 0x40; PORTB = 0xBF;
        // set SPCR register to enable SPI and enable SPI interrupt (pg. 168)
        SPCR |= (1<<SPE) | (1<<SPIE);
        // make sure global interrupts are enabled on SREG register (pg. 9)
        SREG = 0x80;
}

unsigned receivedData;
ISR(SPI_STC_vect) { // this is enabled in with the SPCR register?s ?SPI
  // Interrupt Enable?
        // SPDR contains the received data, e.g. unsigned char receivedData =
// SPDR;
        receivedData = SPDR;
}

unsigned char temp;
unsigned char pattern;
unsigned char speed;

unsigned char clr_highNibble = 0x0F;
unsigned char clr_lowNibble = 0xF0;
unsigned char btn;
unsigned char data = 0x11;
enum masterStates {m_read};
int TickFct_master( int state ) {
        switch( state ) {
                case -1:
                        state = m_read;
                        break;
                case m_read:
                        state = m_read;
                        break;
                default:
                        break;          
        }
        
        switch( state ) {
                case m_read:
                        btn = GetKeypadKey();
                        switch( btn ) {
                                case 'A':
                                        data = data & clr_highNibble;
                                        data = data | 0x10;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case 'B':
                                        data = data & clr_highNibble;
                                        data = data | 0x20;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case 'C':
                                        data = data & clr_highNibble;
                                        data = data | 0x30;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case 'D':
                                        data = data & clr_highNibble;
                                        data = data | 0x40;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '1':
                                        data = data & clr_lowNibble;
                                        data = data | 0x01;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '2':
                                        data = data & clr_lowNibble;
                                        data = data | 0x02;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '3':
                                        data = data & clr_lowNibble;
                                        data = data | 0x03;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '4':
                                        data = data & clr_lowNibble;
                                        data = data | 0x04;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '5':
                                        data = data & clr_lowNibble;
                                        data = data | 0x05;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                case '6':
                                        data = data & clr_lowNibble;
                                        data = data | 0x06;
                                        PORTA = SetBit(PORTA,2,1);
                                        break;
                                default:
                                        PORTA = SetBit(PORTA,2,0);
                                        break;
                        }
                        temp = data;
                        speed = temp & 0x0F;
                        pattern = (temp >> 4) & 0x0F;
                        LCD_DisplayString(1,"Ptrn: ");
                        LCD_WriteData(pattern + '0');
                        LCD_DisplayString(9,"Spd: ");
                        LCD_WriteData(speed + '0');
                        SPI_MasterTransmit(data);
                        SPI_MasterTransmit(data);
                        break;
                default:
                        break;
        }
        
        return state;
}

enum servantStates {s_wait, read};
int TickFct_servant( int state ) {
        switch( state )  {
                case -1:
                        state = s_wait;
                        break;
                case s_wait:
                        state = read;
                        break;
                case read:
                        state = s_wait;
                        break;
                default:
                        state = s_wait;
                        break;
        }
        
        switch( state ) {
                case s_wait:
                        PORTA = SetBit(PORTA,0,0);
                        unsigned int j;
                        switch(speed) {
                                case 1:
                                        for (j = 0; j < tasksNum; j++) { 
                                                tasks[j].period = 2000;
                                        }                                                               
                                        break;
                                case 2:
                                        for (j = 0; j < tasksNum; j++) { 
                                                        tasks[j].period = 1000;
                                        }       
                                        break;
                                case 3:
                                        for (j = 0; j < tasksNum; j++) {
                                                tasks[j].period = 500;
                                        }
                                        break;
                                case 4:
                                        for (j = 0; j < tasksNum; j++) {
                                                tasks[j].period = 250;
                                        }
                                        break;
                                case 5:
                                        for (j = 0; j < tasksNum; j++) {
                                                tasks[j].period = 100;
                                        }
                                        break;
                                case 6:
                                        for (j = 0; j < tasksNum; j++) {
                                                tasks[j].period = 50;
                                        }
                                        break;
                                default:
                                        break;  
                        }                                               
                        break;
                case read:
                        PORTA = SetBit(PORTA,0,1);
                        temp = receivedData;
                        speed = temp & 0x0F;
                        pattern = (temp >> 4) & 0x0F;
                        break;
                default:
                        break;
        }
        
        return state;
}

enum pattern1States {wait1, light1, light2};
int TickFct_pattern1( int state ) {
        switch( state ) {
                case -1:
                        state = wait1;
                        break;
                case wait1:
                        if(pattern == 1)
                                state = light1;
                        else
                                state = wait1;
                        break;
                case light1:
                        if(pattern != 1)
                                state = wait1;
                        else if(pattern == 1)
                                state = light2;
                        break;
                case light2:
                        if(pattern != 1)
                                state = wait1;
                        else if(pattern == 1)
                                state = light1;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case wait1:
                        break;
                case light1:
                        PORTC = pattern | 0x0E;
                        break;
                case light2:
                        PORTC = 0xF0;
                        break;
                default:
                        break;
        }
        
        return state;
}

enum pattern2States {wait2, light3, light4};
int TickFct_pattern2( int state ) {
        switch( state ) {
                case -1:
                        state = wait2;
                        break;
                case wait2:
                        if(pattern == 2)
                                state = light3;
                        else
                                state = wait2;
                        break;
                case light3:
                        if(pattern != 2)
                                state = wait2;
                        else if(pattern == 2)
                                state = light4;
                        break;
                case light4:
                        if(pattern != 2)
                                state = wait2;
                        else if(pattern == 2)
                                state = light3;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case wait2:
                        break;
                case light3:
                        PORTC = pattern | 0xA8;
                        break;
                case light4:
                        PORTC = 0x55;
                        break;
                default:
                        break;
        }
        
        return state;
}

unsigned char pattern3 = 0x01;
enum pattern3States {wait3, shift_left, shift_right};
int TickFct_pattern3( int state ) {
        switch( state ) {
                case -1:
                        state = wait3;
                        break;
                case wait3:
                        if(pattern == 3) {
                                pattern3 = 0x80;
                                state = shift_right;
                        }                       
                        else
                                state = wait3;
                        break;
                case shift_left:
                        if(pattern == 3 && pattern3 != 0x80)
                                state = shift_left;
                        else if(pattern ==3 && pattern3 == 0x80)
                                state = shift_right;
                        else if(pattern != 3) 
                                state = wait3;
                        break;
                case shift_right:
                        if(pattern == 3 && pattern3 != 0x01)
                                state = shift_right;
                        else if(pattern == 3 && pattern3 == 0x01)
                                state = shift_left;
                        else if(pattern != 3)
                                state = wait3;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case wait3:
                        pattern3 = 0x00;
                        break;
                case shift_left:
                        pattern3 = pattern3 << 1;
                        PORTC = pattern3;
                        break;
                case shift_right:
                        pattern3 = pattern3 >> 1;
                        PORTC = pattern3;
                        break;
                default:
                        break;
        }
        
        return state;
}

unsigned char pattern4 = 0xF0;
enum pattern4States {wait4, shift_left4, shift_right4};
int TickFct_pattern4( int state ) {
        switch( state ) {
                case -1:
                        state = wait4;
                        break;
                case wait4:
                        if(pattern == 4)
                                state = shift_right4;
                        else
                                state = wait4;
                        break;
                case shift_left4:
                        if(pattern == 4 && pattern4 != 0xF0)
                                state = shift_left4;
                        else if(pattern == 4 && pattern4 == 0xF0)
                                state = shift_right4;
                        else if(pattern != 4) 
                                state = wait4;
                        break;
                case shift_right4:
                        if(pattern == 4 && pattern4 != 0x0F)
                                state = shift_right4;
                        else if(pattern == 4 && pattern4 == 0x0F)
                                state = shift_left4;
                        else if(pattern != 4)
                                state = wait4;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case wait4:
                        break;
                case shift_left4:
                        pattern4 = pattern4 << 1;
                        PORTC = pattern4;
                        break;
                case shift_right4:
                        pattern4 = pattern4 >> 1;
                        PORTC = pattern4;
                        break;
                default:
                        break;
        }
        
        return state;
}

enum LCDStates {LCD_read};
int TickFct_LCD( int state ) {
        switch( state ) {
                case -1:
                        state = LCD_read;
                        break;
                case LCD_read:
                        state = LCD_read;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case LCD_read:
                        
                        break;
                default:
                        break;
        }
        return state;
}

int main(void)
{
        // MASTER
        /*DDRC = 0xF0; PORTC = 0x0F;
        DDRD = 0xFF; PORTD = 0x00;
        DDRA = 0xFF; PORTA = 0x00;
        SPI_MasterInit();
                
        tasksNum = 1; // declare number of tasks
        task tsks[1]; // initialize the task array
        tasks = tsks; // set the task array
                
        // define tasks
        unsigned char i=0; // task counter
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_master;*/
        
        //SLAVE
        SPI_ServantInit();
        DDRC = 0xFF; PORTC = 0x00;
        DDRA = 0xFF; PORTA = 0x00;
                                
        tasksNum = 5; // declare number of tasks
        task tsks[5]; // initialize the task array
        tasks = tsks; // set the task array
        
        // define tasks
        unsigned char i=0; // task counter
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_servant;
                i++;
                
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_pattern1;
        i++;
                
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_pattern2;
        i++;
                
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_pattern3;
        i++;
                
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_pattern4;
        
        TimerSet(100);
        TimerOn();
                
        LCD_init();
        
    while(1)
    {
        //TODO:: Please write your application code 
                //SetBit(PORTA,2,1);
    }
}