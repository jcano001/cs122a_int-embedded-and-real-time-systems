/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #6 Exercise #2

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
// The following is sample code for how to create a main.c that works with scheduler.h
// This program will cause D0 to blink on and off every 1500ms. D4D5D6 will rotate every 500ms.
// This code will work with the ATMega1284 or ATMega32

#include <avr/io.h>
#include "scheduler.h"

void A2D_init() {
	ADCSRA |= (1 << ADEN) | (1 << ADSC) | (1 << ADATE);
	// ADEN: Enables analog-to-digital conversion
	// ADSC: Starts analog-to-digital conversion
	// ADATE: Enables auto-triggering, allowing for constant
	//	    analog to digital conversions.
}


unsigned char pattern = 0x80;//[4] = {0x20,0x10,0x08,0x04};// LED pattern - 0: LED off; 1: LED on
unsigned char row[4] = {0xF1,0xF5,0xF5,0xF1};// Row(s) displaying pattern.
// 0: display pattern on row
// 1: do NOT display pattern on row


unsigned short Vref;
enum readBtn_States {btn,left,right};
int readBtn_Tick(int state) {
	unsigned int i = 0;
	
	unsigned short Vin = ADC;

	switch(state) {
		case btn:
			Vin = ADC;
			if( Vin < Vref - 112 )
				state = left;
			else if ( Vin > Vref + 112 )
				state = right;
			else
				state = btn;
			break;
		case right:
			Vin = ADC;
			if( Vin > Vref + 112)
				state = right;
			else
				state = btn;
			break;
		case left:
			Vin = ADC;
			if( Vin < Vref - 112)
				state = left;
			else
				state = btn;
			break;
		default:
			state = btn;
			break;
	}

	switch(state) {
		case btn:
			break;
		case left:
			if( pattern == 0x80 ) {
				pattern = 0x01;
				break;
			}				
			pattern = pattern << 1;
			break;
		case right:
			if( pattern == 0x01 ) {
				pattern = 0x80;
				break;
			}			
			pattern = pattern >> 1;
			break;
		default:
			break;
		}

	return state;
}

//--------------------------------------
// LED Matrix Demo SynchSM
// Period: 100 ms
//--------------------------------------
enum Demo_States {init,s1};
int Demo_Tick(int state) {
	// Local Variables
	static unsigned char p;
	// Transitions
	switch (state) {
		case init:
			state = s1;
			break;
		case s1:
			state = s1;
			break;
		default:
			state = init;
			break;
	}

	// Actions
	switch (state) {
		case init:
			break;
		case s1:
			p = pattern;
			break;
		default:
			break;
	}

	PORTC = p; // Pattern to display
	PORTD = ~0x01; // Row(s) displaying pattern

	return state;
}



int main(void) {
	A2D_init();
	
	// initialize ports
	DDRA = 0x00; PORTA = 0xFF;
	DDRC = 0xFF; PORTC = 0x00;
	DDRD = 0xFF; PORTD = 0x00;

	tasksNum = 2; // declare number of tasks
	task tsks[2]; // initialize the task array
	tasks = tsks; // set the task array

	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 1;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &Demo_Tick;
	i++;

	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &readBtn_Tick;
	TimerSet(1); // value set should be GCD of all tasks
	TimerOn();

	Vref = ADC;
	
	while(1) {} // task scheduler will be called by the hardware interrupt

}