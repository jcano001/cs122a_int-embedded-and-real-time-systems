/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #5 Exercise #4

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
// The following is sample code for how to create a main.c that works with scheduler.h
// This program will cause D0 to blink on and off every 1500ms. D4D5D6 will rotate every 500ms.
// This code will work with the ATMega1284 or ATMega32

#include <avr/io.h>
#include "scheduler.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

signed char go = -1;
unsigned char power = 1;

// Ensure DDRC is setup as output
void transmit_data(unsigned char data) {
unsigned char i;
for (i = 0; i < 8 ; ++i) {
// Sets SRCLR to 1 allowing data to be set
// Also clears SRCLK in preparation of sending data
PORTC = 0x08;
// set SER = next bit of data to be sent.
PORTC |= ((data >> i) & 0x01);
// set SRCLK = 1. Rising edge shifts next bit of data into the shift register
PORTC |= 0x04;
}
// set RCLK = 1. Rising edge copies data from the ???Shift??? register to the ???Storage??? register
PORTC |= 0x02;
// clears all lines in preparation of a new transmission
PORTC = 0x00;
}

unsigned char pattern[4] = {0x20,0x10,0x08,0x04};// LED pattern - 0: LED off; 1: LED on
unsigned char row[4] = {0xF1,0xF5,0xF5,0xF1};// Row(s) displaying pattern. 
											// 0: display pattern on row
											// 1: do NOT display pattern on row

enum readBtn_States {btn,left,right,up,down,hold_left,hold_right,hold_up,hold_down};
int readBtn_Tick(int state) {
	unsigned int i = 0;
	
	switch(state) {
		case btn:
			if(!GetBit(PINA,0))
				state = hold_up;
			else if(!GetBit(PINA,1))
				state = hold_down;
			else if(!GetBit(PINA,2))
				state = hold_right;
			else if(!GetBit(PINA,3))
				state = hold_left;
			else
				state = btn;
			break;
		case hold_up:
			if(!GetBit(PINA,0))
				state = hold_up;
			else state = up;
			break;
		case hold_down:
			if(!GetBit(PINA,1))
				state = hold_down;
			else state = down;
			break;
		case hold_right:
			if(!GetBit(PINA,2))
				state = hold_right;
			else state = right;
			break;
		case hold_left:
			if(!GetBit(PINA,3))
				state = hold_left;
			else state = left;
			break;
		case up:
			state = btn;
			break;
		case down:
			state = btn;
			break;
		case right:
			state = btn;
			break;
		case left:
			state = btn;
			break;
		default:
			state = btn;
			break;
	}
	
	switch(state) {
		case btn:
			break;
		case hold_up:
			break;
		case hold_down:
			break;
		case hold_right:
			break;
		case hold_left:
			break;
		case left:
			if( pattern[0] != 0x80 ) {
				for(i = 0; i < 4; i++) {
					pattern[i] = pattern[i] << 1;
				}
			}			
			break;
		case right:
			if( pattern[3] != 0x01 ) {
				for(i = 0; i < 4; i++) {
					pattern[i] = pattern[i] >> 1;
				}
			}
			break;
		case up:
			if( GetBit(PORTD,0) ) {
				for(i = 0; i < 4; i++) {
					row[i] = ~(~row[i] >> 1);
				}
			}
			break;
		case down:
			if( GetBit(PORTD,4) ) {
				for(i = 0; i < 4; i++) {
					row[i] = ~(~row[i] << 1);
				}
			}
			break;
		default:
			break;
		
	}
	
	return state;
}

//--------------------------------------
// LED Matrix Demo SynchSM
// Period: 100 ms
//--------------------------------------
enum Demo_States {init,s1,s2,s3,s4};
int Demo_Tick(int state) {

	// Local Variables
	static unsigned char p,r;
	// Transitions
	switch (state) {
		case init:
			state = s1;
			break;
		case s1:
			state = s2;
			break;
		case s2:
			state = s3;
			break;
		case s3:
			state = s4;
			break;
		case s4:
			state = s1;
			break;
		default:
			state = init;
			break;
	}
	
	// Actions
	switch (state) {
		case init:
			break;
		case s1:
			p = pattern[0];
			r = row[0];
			break;
		case s2:
			p = pattern[1];
			r = row[1];
			break;
		case s3:
			p = pattern[2];
			r = row[2];
			break;
		case s4:
			p = pattern[3];
			r = row[3];
			break;
		default:
			break;
	}
	
	PORTC = p;	// Pattern to display
	PORTD = r;		// Row(s) displaying pattern
	
	return state;
}


int main(void) {

	// initialize ports
	DDRA = 0x00; PORTA = 0xFF;
	DDRC = 0xFF; PORTC = 0x00;
	DDRD = 0xFF; PORTD = 0x00;

tasksNum = 2; // declare number of tasks
task tsks[2]; // initialize the task array
tasks = tsks; // set the task array

// define tasks
unsigned char i=0; // task counter
tasks[i].state = -1;
tasks[i].period = 1;
tasks[i].elapsedTime = tasks[i].period;
tasks[i].TickFct = &Demo_Tick;
i++;

tasks[i].state = -1;
tasks[i].period = 100;
tasks[i].elapsedTime = tasks[i].period;
tasks[i].TickFct = &readBtn_Tick;
TimerSet(1); // value set should be GCD of all tasks
TimerOn();

   while(1) {} // task scheduler will be called by the hardware interrupt

}