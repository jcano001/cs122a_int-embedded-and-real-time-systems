/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #4 Exercise #2

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
// The following is sample code for how to create a main.c that works with scheduler.h
// This program will cause D0 to blink on and off every 1500ms. D4D5D6 will rotate every 500ms.
// This code will work with the ATMega1284 or ATMega32

#include <avr/io.h>
#include "scheduler.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

signed char go = -1;
unsigned char power = 1;

// Ensure DDRC is setup as output
void transmit_data(unsigned char data) {
unsigned char i;
for (i = 0; i < 8 ; ++i) {
// Sets SRCLR to 1 allowing data to be set
// Also clears SRCLK in preparation of sending data
PORTC = 0x08;
// set SER = next bit of data to be sent.
PORTC |= ((data >> i) & 0x01);
// set SRCLK = 1. Rising edge shifts next bit of data into the shift register
PORTC |= 0x04;
}
// set RCLK = 1. Rising edge copies data from the ???Shift??? register to the ???Storage??? register
PORTC |= 0x02;
// clears all lines in preparation of a new transmission
PORTC = 0x00;
}

//--------------------------------------
// LED Matrix Demo SynchSM
// Period: 100 ms
//--------------------------------------
enum Demo_States {init,shift_up, shift_down, wait_up, wait_down};
int Demo_Tick(int state) {

	// Local Variables
	static unsigned char pattern = 0xFF;	// LED pattern - 0: LED off; 1: LED on
	static unsigned char row = 0xFE;  	// Row(s) displaying pattern. 
							// 0: display pattern on row
							// 1: do NOT display pattern on row
	// Transitions
	switch (state) {
		case init:	
			if( !GetBit(PINA,0) )
				state = wait_up;
			else if ( !GetBit(PINA,1) )
				state = wait_down;
			else
				state = init;
				if( !GetBit(PORTD,4) )
				row = 0xEF;
				if( !GetBit(PORTD,0) )
				row = 0xFE;
			break;
		case wait_up:
			if( !GetBit(PINA,0) )
				state = wait_up;
			else
				state = shift_up;
			break;
		case wait_down:
			if( !GetBit(PINA,1) )
				state = wait_down;
			else
				state = shift_down;
			break;
		case shift_up:
			state = init;
			break;
		case shift_down:
			state = init;
			break;
		default:
			state = init;
				break;
	}
	
	// Actions
	switch (state) {
		case init:
			break;
		case wait_up:
			
			break;
		case wait_down:
			
			break;
		case shift_up:
			if( GetBit(PORTD,0) )
				row = ~(~row >> 1);
			break;
		case shift_down:
			if( GetBit(PORTD,4) )
				row = ~(~row << 1);
			break;		
		default:
			break;
	}
	
	PORTC = pattern;	// Pattern to display
	PORTD = row;		// Row(s) displaying pattern
	
	return state;
}


int main(void) {

	// initialize ports
	DDRA = 0x00; PORTA = 0xFF;
	DDRC = 0xFF; PORTC = 0x00;
	DDRD = 0xFF; PORTD = 0x00;

tasksNum = 1; // declare number of tasks
task tsks[1]; // initialize the task array
tasks = tsks; // set the task array

// define tasks
unsigned char i=0; // task counter
tasks[i].state = -1;
tasks[i].period = 100;
tasks[i].elapsedTime = tasks[i].period;
tasks[i].TickFct = &Demo_Tick;

TimerSet(100); // value set should be GCD of all tasks
TimerOn();

   while(1) {} // task scheduler will be called by the hardware interrupt

}