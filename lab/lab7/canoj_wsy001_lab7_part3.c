/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #7 Exercise #2

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "scheduler.h"
#include "usart_ATmega1284.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
	return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
	return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

unsigned char isFollower = 1; // initially follower

enum Leader_States {waitLeader,on,off};
int Leader_Tick(int state) {
        switch(state) {
                case -1:
                        state = waitLeader;
                        break;
                case waitLeader:
						if( isFollower )
							state = waitLeader;
						else
							state = on;
                        break;
                case on:
                        if( !isFollower )
                            state = off;
                        else
                            state = waitLeader;
                        break;
                case off:
                        if( !isFollower )
                            state = on;
                        else
							state = waitLeader;
                        break;
                default:
                        state = waitLeader;
                        break;
        }

        switch(state) {
                case waitLeader:
                        break;
                case on:
                        PORTC = 0x01;
                        if( USART_IsSendReady(1) ) {
                                USART_Send(0x01,1);
                                PORTA = 0x01;
                        }
                        break;
                case off:
                        if( USART_IsSendReady(1) ) {
                                USART_Send(0x00,1);
                                PORTA = 0x00;
                        }
                        break;
                default:
                        break;
        }

        return state;
}

unsigned char j = 0;
enum Follower_States {waitFollower,receive};
int Follower_Tick(int state) {
        switch(state) {
                case -1:
                        state = waitFollower;
                        break;
                case waitFollower:
						if( !USART_HasReceived(0) && j >= 30 ) { // becomes leader
							state = waitFollower;
							isFollower = 0;
						}						
						else {
							state = receive;
							isFollower = 1;
						}						
                        break;
                case receive:
						if( j < 30 ) {
							state = receive;
						}
						else
							state = waitFollower;						
                        break;
                default:
							state = waitFollower;
                        break;
        }

        switch(state) {
                case waitFollower:
                        break;
                case receive:
						PORTC = 0x00;
                        if( USART_HasReceived(0) ) {
                                PORTA = USART_Receive(0);
                                USART_Flush(0);
								j = 0;
                        }
						else {
							j++;
						}						
                        break;
                default:
                        break;
        }

        return state;
}

int main(void)
{
    //TODO:: Please write your application code
        initUSART(0);
        initUSART(1);
        USART_Flush(0);
        USART_Flush(1);
        
        // initialize ports
        DDRB = 0x00; PORTB = 0xFF;
                
        DDRA = 0xFF; PORTA = 0x00;
        DDRC = 0xFF; PORTC = 0x00;

        tasksNum = 2; // declare number of tasks
        task tsks[2]; // initialize the task array
        tasks = tsks; // set the task array

        // define tasks
        unsigned char i=0; // task counter
        tasks[i].state = -1;
        tasks[i].period = 500;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &Leader_Tick;
        i++;
        
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &Follower_Tick;
                
        TimerSet(1);
        TimerOn();
        
        while(1) {}
        
    return 0;
}