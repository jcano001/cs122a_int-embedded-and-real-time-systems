/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #7 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "scheduler.h"
#include "usart_ATmega1284.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
	return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
	return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

enum Follower_States {init,on};
int Follower_Tick(int state) {
        switch(state) {
                case -1:
                        state = init;
                        break;
                case init:
                        state = on;
                        break;
                case on:
                        state = on;
                        break;
                default:
                        state = init;
                        break;
        }

        switch(state) {
                case init:
                        break;
                case on:
                        if( USART_HasReceived(0) ) {
                                PORTA = USART_Receive(0);
								USART_Flush(0);
                        }
                        break;
                default:
                        break;
        }

        return state;
}

int main(void)
{
    //TODO:: Please write your application code
        initUSART(0);
        
        // initialize ports
        DDRA = 0xFF; PORTA = 0x00;

        tasksNum = 1; // declare number of tasks
        task tsks[1]; // initialize the task array
        tasks = tsks; // set the task array

        // define tasks
        unsigned char i=0; // task counter
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &Follower_Tick;
        
        TimerSet(1);
        TimerOn();
        
        while(1) {}
        
    return 0;
}