/* Partner(s) Name & E-mail:
Justin Cano (jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #2 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/



#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

typedef struct task {
	int state;
	unsigned long period;
	unsigned long elapsedTime;
	int (*TickFct)(int);
} task;

unsigned char GetBit(unsigned char x, unsigned char k)
{
	return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
	return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}


task tasks[2];

const unsigned char tasksNum = 2;
const unsigned long getInputPeriod = 100;
const unsigned long decodePeriod = 100;
const unsigned long tasksPeriodGCD = 100;

int TickFct_getInput(int state);
int TickFct_decode(int state);


void TimerOn(){
	TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);    //(ClearTimerOnCompare mode). //Prescaler=?
	TIMSK1 = (1<<OCIE1A);    //Enables compare match interrupt
	SREG |= 0x80;   //Enable global interrupts
}

void TimerSet(int ms){
	TCNT1 = 0;      //Clear the timer counter
	OCR1A = 125 * ms;       //Set the match compare value
}

void TimerISR() {
	unsigned char i;
	for (i = 0; i < tasksNum; ++i) { // Heart of the scheduler code
	if ( tasks[i].elapsedTime >= tasks[i].period ) { // Ready
		tasks[i].state = tasks[i].TickFct(tasks[i].state);
		tasks[i].elapsedTime = 0;
		}
	tasks[i].elapsedTime += tasksPeriodGCD;
	}
}

ISR(TIMER1_COMPA_vect){
     //RIOS
     TimerISR();
}

/*TODO: Define GLOBAL user variables and functions here.*/
unsigned char count = 0;
unsigned char hold_count = 0;
unsigned char display_count = 0;
unsigned char flag = 0;
unsigned char array[];
unsigned char display = 0;
unsigned char arraySize = 0;
unsigned char * pointer = array;

void morse(int hash) {
	switch(hash){
		case -1: // no input yet
			break;
		case 0: // S
			PORTD = 0x53;
			break;
		case 3: // O
			PORTD = 0x4F;
			break;
		default:
			PORTD = 0xFF;
			break;
	}
}

int hashMorseInput(char * input, int size) {
	if(size == 0) return -1;
	unsigned int i = 0;
	unsigned int hash = 0;
	for( ; i < size; i++){
		hash += input[i];
	}		
	return hash % 36;
}


int main() {
	/*TODO: Set DDR ports (0x00 for input, 0xFF for output)*/
	DDRA = 0x00;//00 is input
	PORTA = 0xFF;
	DDRD = 0xFF;//ff out
	PORTD = 0x00;

	/*TODO: Set up state machines*/
	unsigned char i=0;
	tasks[i].state = -1;
	tasks[i].period = getInputPeriod;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_getInput;
	++i;
	
	tasks[i].state = -1;
	tasks[i].period = decodePeriod;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_decode;
	++i;
	
	TimerSet(tasksPeriodGCD);
	TimerOn();

	while(1) {};
		
	return 0;
}


/*TODO: Define state machines*/

enum getInput_states{wait, btnPress, dash, dot, done};
	
int TickFct_getInput(int state) {
	switch(state) { //transitions
		case -1:
			state = wait;
			break;
		case wait:
			if( (GetBit(PINA,0)) && (count < 10) ) {
				state = wait;
				count++;
			}				
			else if( !(GetBit(PINA,0)) ) {
				state = btnPress;
				hold_count = 0;
			}
			else if( (GetBit(PINA,0)) && (count >= 10) && (arraySize > 0))	{
				state = done;
				display = 1;
				PORTD = 0x00;
			}
			break;
		case btnPress:
			if( !(GetBit(PINA,0)) ) {
				state = btnPress;
				hold_count++;
			}
			else if( (GetBit(PINA,0)) && (hold_count > 3) ) {
				state = dash;
			}
			else if( (GetBit(PINA,0)) && (hold_count < 4) ) {
				state = dot;
			}
			else PORTD = 0xFF;
			break;
		case dash:
			state = wait;
			count = 0;
			break;
		case dot:
			state = wait;
			count = 0;
			break;
		case done:
			if(display) state = done;
			else{
				state = wait;
				count = 0;
				pointer = array;
				arraySize = 0;
			}				
			break;
		default:
			state = -1;
			break;
	}
	
	switch(state) { //actions
		case wait:
			flag = 0;
			PORTD = SetBit(PORTD, 7, 0);
			break;
		case btnPress:
			PORTD = 0x00;
			PORTD = SetBit(PORTD, 7, 1);
			break;
		case dash:
			PORTD = 0x00;
 			(*pointer) = '1';
 			pointer++;
			PORTD = SetBit(PORTD,0,1);
			PORTD = SetBit(PORTD,1,1);
			PORTD = SetBit(PORTD,2,1);
			arraySize++;
			break;
		case dot:
			PORTD = 0x00;
 			(*pointer) = '0';
 			pointer++;
			PORTD = SetBit(PORTD,0,1);
			arraySize++;
			break;
		case done:
			flag = 1;
// 			PORTD = SetBit(PORTD,6,array[0]);
// 			PORTD = SetBit(PORTD,3,array[1]);
// 			PORTD = SetBit(PORTD,0,array[2]);
			(*pointer) = '\0';
			break;
		default:
			break;
	}
	return state;
}

enum decode_states{decodeWait, displayLED};
	
int TickFct_decode(int state) {
	switch(state) { //transitions
		case -1:
			state = decodeWait;
			break;
		case decodeWait:
			if(display) state = displayLED;
			else state = decodeWait;
			break;
		case displayLED:
			if(display_count < 50) state = displayLED;
			else if(display_count >= 50) {
				state = decodeWait;
				display = 0;
				PORTD = 0x00;
			}				
			break;
		default:
			state = decodeWait;
			break;
	}
	
	switch(state) { //actions
		case decodeWait:
			display_count = 0;
			break;
		case displayLED:
			display_count++;
			morse(hashMorseInput(array,arraySize));
			break;
		default:
			break;
	}
	return state;
}