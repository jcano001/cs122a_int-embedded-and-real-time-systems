/*
* GccApplication2.cpp
*
* Created: 10/8/2013 12:19:28 PM
*
*/

/* Partner(s) Name & E-mail:Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #2 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/

#include <avr/io.h>
#include <avr/interrupt.h>



typedef struct task {
	int state;
	unsigned long period;
	unsigned long elapsedTime;
	int (*TickFct)(int);
} task;

task tasks[3];

const unsigned char tasksNum = 3;
const unsigned long periodled1 = 500;
const unsigned long periodled2 = 1000;
const unsigned long periodled3 = 2500;

const unsigned long tasksPeriodGCD = 500;


int TickFct_led1(int state);
int TickFct_led2(int state);
int TickFct_led3(int state);





void TimerOn(){
	TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);    //(ClearTimerOnCompare mode). //Prescaler=?
	TIMSK1 = (1<<OCIE1A);    //Enables compare match interrupt
	SREG |= 0x80;   //Enable global interrupts
}

void TimerSet(int ms){
	TCNT1 = 0;      //Clear the timer counter
	OCR1A = 125 * ms;       //Set the match compare value
}

void TimerISR() {
	unsigned char i;
	for (i = 0; i < tasksNum; ++i) { // Heart of the scheduler code
	if ( tasks[i].elapsedTime >= tasks[i].period ) { // Ready
	tasks[i].state = tasks[i].TickFct(tasks[i].state);
	tasks[i].elapsedTime = 0;
}
tasks[i].elapsedTime += tasksPeriodGCD;
}
}

ISR(TIMER1_COMPA_vect){
       //RIOS
       TimerISR();
}

int main() {
       
       DDRD = 0xFF; PORTD = 0x00;
       
       // Priority assigned to lower position tasks in array
       unsigned char i=0;
       tasks[i].state = -1;
       tasks[i].period = periodled1;
       tasks[i].elapsedTime = tasks[i].period;
       tasks[i].TickFct = &TickFct_led1;

       ++i;
       tasks[i].state = -1;
       tasks[i].period = periodled2;
       tasks[i].elapsedTime = tasks[i].period;
       tasks[i].TickFct = &TickFct_led2;

       ++i;
       tasks[i].state = -1;
       tasks[i].period = periodled3;
       tasks[i].elapsedTime = tasks[i].period;
       tasks[i].TickFct = &TickFct_led3;

       ++i;
       TimerSet(tasksPeriodGCD);
       TimerOn();
       
while(1) { }

return 0;
}


//LED1
//transitions
enum led1_states{led1_on, led1_off} ;
       
int TickFct_led1(int state){
       
       switch(state){
               case -1:
                       state = led1_on;
                       break;
                       
               case led1_on:
                       state = led1_off;
                       break;
                       
               case led1_off:
                       state = led1_on;
                       break;
                       
               default:
                       state = -1;
       }
// transitions

//state actions
       switch(state){
               case led1_on:
               PORTD = PORTD ^ (0x01);
               break;
               
               case led1_off:
               PORTD = PORTD ^ (0x01);
               break;
               
               default:
               break;
       }
       return state;
}

//LED2
//transitions
enum led2_states{led2_on, led2_off};
       
int TickFct_led2(int state){
       
       switch(state){
               case -1:
                       state = led2_on;
                       break;
                       
               case led2_on:
                       state = led2_off;
                       break;
                       
               case led2_off:
                       state = led2_on;
                       break;
                       
               default:
                       state = -1;
       }
// transitions

//state actions
       switch(state){
               case led2_on:
                       PORTD = PORTD ^ (0x08);
                       break;
               
               case led2_off:
                       PORTD = PORTD ^ (0x08);
                       break;
               
               default:
                       break;
       }
       return state;
}

//LED3
//transitions
enum led3_states{led3_on, led3_off} ;
       
int TickFct_led3(int state){
       
       switch(state){
               case -1:
                       state = led3_on;
                       break;
                       
               case led3_on:
                       state = led3_off;
                       break;
                       
               case led3_off:
                       state = led3_on;
                       break;
                       
               default:
                       state = -1;
       }
// transitions

//state actions
       switch(state){
               case led3_on:
                       PORTD = PORTD ^ (0x40);
                       break;
               
               case led3_off:
                       PORTD = PORTD ^ (0x40);
                       break;
               
               default:
                       break;
       }
       return state;
}