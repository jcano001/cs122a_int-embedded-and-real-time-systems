/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #9 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "scheduler.h"
#include "bit.h"

int numPhases = 0;
unsigned char go = 0; // set to 1 for clockwise, 2 for counter-clockwise
int count = 0;

enum readBtnStates {btn_wait,btn_read};
int TickFct_readBtn( int state ) {
	switch( state )	{
		case -1:
			state = btn_wait;
			break;
		case btn_wait:
			if( !GetBit(PINB,0) && go == 0 ) {
				numPhases = (90/5.625) * 64;
				go = 1;
				state = btn_read;
			}
			else if( !GetBit(PINB,1) && go == 0 ) {
				numPhases = (180/5.625) * 64;
				go = 1;
				state = btn_read;
			}
			else if( !GetBit(PINB,2) && go == 0 ) {
				numPhases = (360/5.625) * 64;
				go = 1;
				state = btn_read;
			}
			else if( !GetBit(PINB,3) && go == 0 ) {
				numPhases = (90/5.625) * 64;
				go = 2;
				state = btn_read;
			}
			else if( !GetBit(PINB,4) && go == 0 ) {
				numPhases = (180/5.625) * 64;
				go = 2;
				state = btn_read;
			}
			else if( !GetBit(PINB,5) && go == 0 ) {
				numPhases = (360/5.625) * 64;
				go = 2;
				state = btn_read;
			}							
			else 			
				state = btn_wait;		
			break;
		case btn_read:
			if( !GetBit(PINB,0) ) 
				state = btn_read;
			else if( !GetBit(PINB,1) )
				state = btn_read;
			else if( !GetBit(PINB,2) )
				state = btn_read;
			else if( !GetBit(PINB,3) ) 
				state = btn_read;
			else if( !GetBit(PINB,4) )
				state = btn_read;
			else if( !GetBit(PINB,5) )
				state = btn_read;
			else
				state = btn_wait;
			break;
		default:
			state = btn_wait;
			break;
	}
	
	switch( state ) {
		case btn_wait:
			PORTA = SetBit(PORTA,4,0);
			break;
		case btn_read:
			PORTA = SetBit(PORTA,4,1);
			break;
		default:
			break;
	}
	
	return state;
}

enum motorStates { A, AB, B, BC, C, CD, D, DA };
int TickFct_motor( int state ) {
	switch( state ) {
		case -1:
			state = A;
			break;
		case A:
			if( (go == 1) && (numPhases > 0) ) {
				state = AB;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = DA;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}			
			break;
		case AB:
			if( (go == 1) && (numPhases > 0)) {
				state = B;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = A;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case B:
			if( (go == 1) && (numPhases > 0) ) {
				state = BC;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = AB;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case BC:
			if( (go == 1) && (numPhases > 0) ) {
				state = C;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = B;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case C:
			if( (go == 1) && (numPhases > 0) ) {
				state = CD;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = BC;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case CD:
			if( (go == 1) && (numPhases > 0) ) {
				state = D;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = C;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case D:
			if( (go == 1) && (numPhases > 0) ) {
				state = DA;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = CD;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
		case DA:
			if( (go == 1) && (numPhases > 0) ) {
				state = A;
				numPhases--;
			}
			else if( (go == 2) && (numPhases > 0) ) {
				state = D;
				numPhases--;
			}
			else {
				state = A;
				numPhases = 0;
				go = 0;
			}
			break;
 		default:
			state = A;
			break;
	}
	
	switch( state ) {
		case A:
			PORTA = 0x01;
			break;
		case AB:
			PORTA = 0x03;
			break;
		case B:
			PORTA = 0x02;
			break;
		case BC:
			PORTA = 0x06;
			break;
		case C:
			PORTA = 0x04;
			break;
		case CD:
			PORTA = 0x0C;
			break;
		case D:
			PORTA = 0x08;
			break;
		case DA:
			PORTA = 0x09;
			break;
		default:
			break;
	}
	
	return state;
}

int main(void)
{
	DDRA = 0xFF; PORTA = 0x00;
	DDRB = 0x00; PORTB = 0xFF;
	
	tasksNum = 2; // declare number of tasks
	task tsks[2]; // initialize the task array
	tasks = tsks; // set the task array
	
	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_readBtn;
 	i++;
	
	tasks[i].state = -1;
	tasks[i].period = 3;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_motor;

	
	TimerSet(1);
	TimerOn();

    while(1)
    {
        //TODO:: Please write your application code 

    }
}