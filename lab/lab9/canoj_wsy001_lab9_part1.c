/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #9 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "scheduler.h"
#include "bit.h"

enum motorStates { A, AB, B, BC, C, CD, D, DA };
int TickFct_motor( int state ) {
	switch( state ) {
		case -1:
			state = A;
			break;
		case A:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = AB;
			else if( !GetBit(PINB,1) )
				state = DA;
			break;
		case AB:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = B;
			else if( !GetBit(PINB,1) )
				state = A;
			break;
		case B:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = BC;
			else if( !GetBit(PINB,1) )
				state = AB;
			break;
		case BC:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = C;
			else if( !GetBit(PINB,1) )
				state = B;
			break;
		case C:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = CD;
			else if( !GetBit(PINB,1) )
				state = BC;
			break;
		case CD:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = D;
			else if( !GetBit(PINB,1) )
				state = C;
			break;
		case D:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = DA;
			else if( !GetBit(PINB,1) )
				state = CD;
			break;
		case DA:
			if( (GetBit(PINB,0) && GetBit(PINB,1)) || (!GetBit(PINB,0) && !GetBit(PINB,1)) )
				state = A;
			else if( !GetBit(PINB,0) )
				state = A;
			else if( !GetBit(PINB,1) )
				state = D;
			break;
 		default:
			state = A;
			break;
	}
	
	switch( state ) {
		case A:
			PORTA = 0x11;
			break;
		case AB:
			PORTA = 0x33;
			break;
		case B:
			PORTA = 0x22;
			break;
		case BC:
			PORTA = 0x66;
			break;
		case C:
			PORTA = 0x44;
			break;
		case CD:
			PORTA = 0xCC;
			break;
		case D:
			PORTA = 0x88;
			break;
		case DA:
			PORTA = 0x99;
			break;
		default:
			break;
	}
	
	return state;
}

int main(void)
{
	DDRA = 0xFF; PORTA = 0x00;
	DDRB = 0x00; PORTB = 0xFF;
	
	tasksNum = 1; // declare number of tasks
	task tsks[1]; // initialize the task array
	tasks = tsks; // set the task array
	
	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 3;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_motor;
	
	TimerSet(1);
	TimerOn();

    while(1)
    {
        //TODO:: Please write your application code 

    }
}