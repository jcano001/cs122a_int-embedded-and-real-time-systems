/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #4 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
// The following is sample code for how to create a main.c that works with scheduler.h
// This program will cause D0 to blink on and off every 1500ms. D4D5D6 will rotate every 500ms.
// This code will work with the ATMega1284 or ATMega32

#include <avr/io.h>
#include "scheduler.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
	return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
	return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

unsigned char shift_data = 0;

// Ensure DDRC is setup as output
void transmit_data(unsigned char data) {
	unsigned char i;
	for (i = 0; i < 8 ; ++i) {
		// Sets SRCLR to 1 allowing data to be set
		// Also clears SRCLK in preparation of sending data
		PORTC = 0x08;
		// set SER = next bit of data to be sent.
		PORTC |= ((data >> i) & 0x01);
		// set SRCLK = 1. Rising edge shifts next bit of data into the shift register
		PORTC |= 0x04;
	}
	// set RCLK = 1. Rising edge copies data from the “Shift” register to the “Storage” register
	PORTC |= 0x02;
	// clears all lines in preparation of a new transmission
	PORTC = 0x00;
}


enum PlusMinus_States { init, plus, minus, hold, both_btn };
int TickFct_PlusMinus(int state) {
	switch(state) { // Transitions
		case -1:
			state = init;
			break;
		case init:
			if( GetBit(PINA,0) && GetBit(PINA,1) )
				state = init;
			else if( !GetBit(PINA,0) && !GetBit(PINA,1) )
				state = both_btn;
			else if( !GetBit(PINA,0) )
				state = plus;
			else if ( !GetBit(PINA,1) )
				state = minus;
			break;
		case plus:
			state = hold;
			break;
		case minus:
			state = hold;
			break;
		case hold:
			if( !GetBit(PINA,0) || !GetBit(PINA,1) )
				state = hold;
			else
				state = init;
			break;
		case both_btn:
			if( !GetBit(PINA,0) || !GetBit(PINA,1) )
				state = both_btn;
			else
				state = init;
			break;
		default:
			state = init;		
	}

	switch(state) { // State actions
		case init:
			//transmit_data(0);
			break;
		case plus:
			if( shift_data < 255 )
				transmit_data(++shift_data);
			break;
		case minus:
			if( shift_data > 0 )
				transmit_data(--shift_data);
			break;
		case hold:
			break;
		case both_btn:
			transmit_data(0);
			break;
		default:
			transmit_data(0);
	}
	
	return state;
}

int main(void) {
	
	// initialize ports
	DDRA = 0x00; PORTA = 0xFF;
	DDRC = 0xFF; PORTC = 0x00;
	
	tasksNum = 1; // declare number of tasks
	task tsks[1]; // initialize the task array
	tasks = tsks; // set the task array
	
	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_PlusMinus;
	
	TimerSet(100); // value set should be GCD of all tasks
	TimerOn();

    while(1) {} // task scheduler will be called by the hardware interrupt
	
}
