/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #4 Exercise #2

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
// The following is sample code for how to create a main.c that works with scheduler.h
// This program will cause D0 to blink on and off every 1500ms. D4D5D6 will rotate every 500ms.
// This code will work with the ATMega1284 or ATMega32

#include <avr/io.h>
#include "scheduler.h"

unsigned char GetBit(unsigned char x, unsigned char k)
{
	return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
	return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}

signed char go = -1;
unsigned char power = 1;

// Ensure DDRC is setup as output
void transmit_data(unsigned char data) {
	unsigned char i;
	for (i = 0; i < 8 ; ++i) {
		// Sets SRCLR to 1 allowing data to be set
		// Also clears SRCLK in preparation of sending data
		PORTC = 0x08;
		// set SER = next bit of data to be sent.
		PORTC |= ((data >> i) & 0x01);
		// set SRCLK = 1. Rising edge shifts next bit of data into the shift register
		PORTC |= 0x04;
	}
	// set RCLK = 1. Rising edge copies data from the “Shift” register to the “Storage” register
	PORTC |= 0x02;
	// clears all lines in preparation of a new transmission
	PORTC = 0x00;
}

enum readBtn_States { init, up_hold, down_hold, both_hold };
int TickFct_readBtn(int state) {
	switch(state) {
		case -1:
			state = init;
			break;
		case init:
			if( GetBit(PINA,0) && GetBit(PINA,1) )
				state = init;
			else if( !GetBit(PINA,0) && GetBit(PINA,1) && power )
				state = up_hold;
			else if( GetBit(PINA,0) && !GetBit(PINA,1) && power )
				state = down_hold;
			else if ( !GetBit(PINA,0) && !GetBit(PINA,1) )
				state = both_hold;
			break;
		case up_hold:
			if( !GetBit(PINA,0) )
				state = up_hold;
			else if( GetBit(PINA,0) ) {
				state = init;
				go++;
				if( go > 2 )
					go = 0;
			}
			break;
		case down_hold:
			if( !GetBit(PINA,1) )
				state = down_hold;
			else if( GetBit(PINA,1) ) {
				state = init;
				go--;
				if( go < 0 )
					go = 2;
			}
			break;
		case both_hold:
			if( !GetBit(PINA,0) && !GetBit(PINA,1) )
				state = both_hold;
			else {
				state = init;
				if( power == 1 ) {
					go = -1;
					power = 0;
					transmit_data(0x00);
				}				
				else {
					go = 0;
					power = 1;
				}					
			}				
			break;
		default:
			state = init;
			break;
		}
	
	switch(state) {
		case init:
			break;
		case up_hold:
			break;
		case down_hold:
			break;
		case both_hold:
			break;
		default:
			break;
	}
	
	return state;
}

enum seq1_States { init1, one, two };
int TickFct_seq1(int state) {
	switch(state) {
		case -1:
			state = init1;
			break;
		case init1:
			if( go != 0 )
				state = init1;
			else if( go == 0 )
				state = one;
			break;
		case one:
			if( go != 0 )
				state = init1;
			else
				state = two;
			break;
		case two:
			if( go!= 0 )
				state = init1;
			else
				state = one;
			break;
		default:
			state = init1;
			break;
	}
	
	switch(state) {
		case init1:
			break;
		case one:
			transmit_data(0xAA);
			break;
		case two:
			transmit_data(0x55);
			break;
		default:
			break;
	}
	
	return state;
}

enum seq2_States { init2, three, four };
int TickFct_seq2(int state) {
	switch(state) {
		case -1:
			state = init2;
			break;
		case init2:
			if( go != 1 )
				state = init2;
			else if( go == 1 )
				state = three;
			break;
		case three:
			if( go != 1 )
				state = init2;
			else
				state = four;
			break;
		case four:
			if( go!= 1 )
				state = init2;
			else
				state = three;
			break;
		default:
			state = init2;
			break;
	}
	
	switch(state) {
		case init2:
			break;
		case three:
			transmit_data(0xF0);
			break;
		case four:
			transmit_data(0x0F);
			break;
		default:
			break;
	}
	
	return state;
}

enum seq3_States { init3, five, six };
int TickFct_seq3(int state) {
	switch(state) {
		case -1:
			state = init3;
			break;
		case init3:
			if( go != 2 )
				state = init3;
			else if( go == 2 )
				state = five;
			break;
		case five:
			if( go != 2 )
				state = init3;
			else
				state = six;
			break;
		case six:
			if( go!= 2 )
				state = init3;
			else
				state = five;
			break;
		default:
			state = init3;
			break;
	}
	
	switch(state) {
		case init3:
			break;
		case five:
			transmit_data(0x87);
			break;
		case six:
			transmit_data(0xE1);
			break;
		default:
			break;
	}
	
	return state;
}

int main(void) {
	
	// initialize ports
	DDRA = 0x00; PORTA = 0xFF;
	DDRC = 0xFF; PORTC = 0x00;
	
	tasksNum = 4; // declare number of tasks
	task tsks[4]; // initialize the task array
	tasks = tsks; // set the task array
	
	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_readBtn;
	++i;
	
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_seq1;
	++i;
	
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_seq2;
	++i;
	
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_seq3;
	
	TimerSet(100); // value set should be GCD of all tasks
	TimerOn();

    while(1) {} // task scheduler will be called by the hardware interrupt
	
}
