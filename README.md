# CS 122A - Intermediate Embedded & Real-Time Systems
## UCR | Fall 2013
> Covers software and hardware design of embedded computing systems. Includes hardware and software codesign, advanced programming paradigms (including state machines and concurrent processes), real-time programming and operating systems, basic control systems, and modern chip and design technologies. Laboratories involve use of microcontrollers, embedded microprocessors, programmable logic and advanced simulation, and debug environments. 

## Final Project
### Home Automation System w/ Smart Alarm Clock
A home automation system that controls lights, drapes, and door lock, with an alarm clock that requires a passcode input following motion detection to shut off

+ Google Doc: [https://drive.google.com/folderview?id=0B-_AI2C_nEwvTmwxY05uWjVzeHM&usp=sharing](https://drive.google.com/folderview?id=0B-_AI2C_nEwvTmwxY05uWjVzeHM&usp=sharing)

+ YouTube Demo: [https://www.youtube.com/watch?v=xi0F5iqQMOs](https://www.youtube.com/watch?v=xi0F5iqQMOs)

## Labs
### lab2
+ Task Scheduler

### lab3
+ Free RTOS

### lab4
+ External Registers

### lab5
+ LED Matrix

### lab6
+ Joystick

### lab7
+ USART

### lab8
+ SPI

### lab9
+ Stepper Motors