/* Partner(s) Name & E-mail:
	Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Lab #2 Exercise #1

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/



#include <avr/io.h>
#include <avr/interrupt.h>

typedef struct task {
	int state;
	unsigned long period;
	unsigned long elapsedTime;
	int (*TickFct)(int);
} task;


task tasks[3];

const unsigned char tasksNum = 3;
const unsigned long morseInputPeriod = 300;
const unsigned long tasksPeriodGCD = 300;

int TickFct_morseInput(int state);
int TickFct_test(int state);


void TimerOn(){
	TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);    //(ClearTimerOnCompare mode). //Prescaler=?
	TIMSK1 = (1<<OCIE1A);    //Enables compare match interrupt
	SREG |= 0x80;   //Enable global interrupts
}

void TimerSet(int ms){
	TCNT1 = 0;      //Clear the timer counter
	OCR1A = 125 * ms;       //Set the match compare value
}

void TimerISR() {
	unsigned char i;
	for (i = 0; i < tasksNum; ++i) { // Heart of the scheduler code
		if ( tasks[i].elapsedTime >= tasks[i].period ) { // Ready
		tasks[i].state = tasks[i].TickFct(tasks[i].state);
		tasks[i].elapsedTime = 0;
		}
	tasks[i].elapsedTime += tasksPeriodGCD;
	}
}

ISR(TIMER1_COMPA_vect){
      //RIOS
      TimerISR();
}

unsigned char GetBit(unsigned char x, unsigned char k)
{
return ((x & (0x01 << k)) != 0);
}

unsigned char SetBit(unsigned char x, unsigned char k, unsigned char b)
{
return (b ? x | (0x01 << k) : x & ~(0x01 << k));
}


/*TODO: Define GLOBAL user variables and functions here.*/

int main() {
	/*TODO: Set DDR ports (0x00 for input, 0xFF for output)*/
	DDRA = 0x00; PORTA = 0xFF; // set DDRA for input, set PORTA to pull down logic
	DDRD = 0xFF; PORTD = 0x00; // set DDRD for output, initialize PORTD to 0
	
	/*TODO: Set up state machines*/
	/*
	unsigned char i=0;
	tasks[i].state = -1;
	tasks[i].period = morseInputPeriod;
	tasks[i].elapsedTime = tasks[j].period;
	tasks[i].TickFct = &TickFct_morseInput;
	++i;
	*/
	//TimerSet(tasksPeriodGCD);
	//TimerOn();

	while(1) {
		if(GetBit(PINA,0)) PORTD = 0x66;
		else PORTD = 0x00;
	}
}


/*TODO: Define state machines*/