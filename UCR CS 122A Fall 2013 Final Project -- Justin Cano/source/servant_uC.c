/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Final Project

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "bit.h"
#include "keypad.h"
#include "lcd.h"
#include "scheduler.h"

// Master code
void SPI_MasterInit(void) {
        // Set DDRB to have MOSI, SCK, and SS as output and MISO as input
        DDRB = 0xBF; PORTB = 0x40;
        // Set SPCR register to enable SPI, enable master, and use SCK frequency
        SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0);
        //   of fosc/16  (pg. 168)
        // Make sure global interrupts are enabled on SREG register (pg. 9)
        SREG =0x80;
}

void SPI_MasterTransmit(unsigned char cData) {
        // set SS low
        PORTB = SetBit(PORTB,4,0);
        // data in SPDR will be transmitted, e.g. SPDR = cData;
        SPDR = cData;
        while(!(SPSR & (1<<SPIF))) { // wait for transmission to complete
                ;
        }
        // set SS high
        PORTB = SetBit(PORTB,4,1);
}

// Servant code
void SPI_ServantInit(void) {
        // set DDRB to have MISO line as output and MOSI, SCK, and SS as input
        //DDRB = 0x40; PORTB = 0xBF;
                DDRB = 0x49; PORTB = 0xB6;
        // set SPCR register to enable SPI and enable SPI interrupt (pg. 168)
        SPCR |= (1<<SPE) | (1<<SPIE);
        // make sure global interrupts are enabled on SREG register (pg. 9)
        SREG = 0x80;
}

unsigned receivedData;
ISR(SPI_STC_vect) { // this is enabled in with the SPCR register?s ?SPI
        // Interrupt Enable?
        // SPDR contains the received data, e.g. unsigned char receivedData =
        // SPDR;
        receivedData = SPDR;
}

enum ALARM_MODES {ON,OFF};
enum DOOR_MODES {UNLOCKED,LOCKED,NONE};
enum DRAPES_DIRECTION {UP,DOWN};
/* DOOR determines whether door is locked or unlocked */
int DOOR = UNLOCKED;
/* DRAPES signifies which direction to open the drapes */
int DRAPES = DOWN;
/* DOOR_CODE is the code entered to lock/unlock door */
const int DOOR_CODE = 2580;
/* ALARM_CODE is the code entered to turn off alarm */
const int ALARM_CODE = 1234;
/* ALARM_CLOCK_MODE determines whether alarm clock functionality is on or not */
/*
	IF ALARM_MODE == ON:
		HOME AUTOMATION STATES IDLE
		ALARM CLOCK STATES ACTIVE
	ELIF ALARM_MODE == OFF:
		HOME AUTOMATION STATES ACTIVE
		ALARM CLOCK STATES IDLE
*/
int ALARM_MODE = OFF;
/* ALARM determines if alarm is going or not 
        ON - ALARM is on; speaker is ringing
        OFF - ALARM is off; speaker is off */
int ALARM = OFF;
/* isAlarmSet is 1 if user has entered time for alarm to go off */
char isAlarmSet = 0;
/* mtn is 1 if motion is detected */
unsigned char mtn = 0;
/* when set, numPhases determines how much motor raises */
int numPhases_drapes = 0;
int numPhases_door = 0;
/* alarmTime is the time for the alarm clock to go off
   currentTime is the current time
   if( currentTime == alarmTime ) ALARM = ON; */
int alarmTime = 0;
int currentTime = 0;


int input = 0;
unsigned char array[4] = {'_','_','_','_'};
int input_time = 0;
int input_door = 0;
unsigned char lhs_time_array[2] = {'0','0'};
unsigned char rhs_time_array[2] = {'0','0'};
unsigned char data = '\0';
unsigned char doorData = '\0';


/*************************************/
/*                                   */
/*      HOME AUTOMATION STATES       */
/*                                   */
/*************************************/

/* setAlarmMode()
 *
 * read in button that will turn on ALARM_MODE
 */
unsigned char btn = '\0';
//1
enum setAlarmModeStates {setAlarmModeWait,setAlarmModeRead};
int TickFct_setAlarmMode( int state ) {
        switch( state ) {
                case -1:
                        state = setAlarmModeWait;
                        break;
                case setAlarmModeWait:
                        if( ALARM_MODE == OFF ) {
                                if( btn == 'b' )
                                        state = setAlarmModeRead;
                                else
                                        state = setAlarmModeWait;
                        }
                        else
                                state = setAlarmModeWait;
						btn = '\0';
                        break;
                case setAlarmModeRead:
						state = setAlarmModeWait;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case setAlarmModeWait:
                        btn = receivedData;
						//PORTB = SetBit(PORTB,0,0);
                        break;
                case setAlarmModeRead:
                        ALARM_MODE = ON;
                        btn = '\0';
						receivedData = '\0';
						//PORTB = SetBit(PORTB,0,1);
                        break;
                default:
                        break;
        }

        return state;   
}


unsigned char isKeypadBusy = 0;
//2
enum inputDoorCodeStates {inputDoorCodeWait, inputDoorCodeRead, inputDoorCodeDecode, inputDoorCodeSuccess, inputDoorCodeFail};
int TickFct_inputDoorCode( int state ) {
        static int i = 0;
        switch( state )  {
                case -1:
                        state = inputDoorCodeWait;
                        break;
                case inputDoorCodeWait:
					if( ALARM_MODE == OFF ) {
						if( doorData == '\0' || doorData == 'b' || doorData == 'u' || doorData == 'd' )
							state = inputDoorCodeWait;
						else if( doorData == '#' )
							state = inputDoorCodeDecode;
						else
							state = inputDoorCodeRead;
					}
					else
						state = inputDoorCodeWait;
					break;
                case inputDoorCodeRead:
                    state = inputDoorCodeWait;
                    break;
                case inputDoorCodeDecode:
						if( input_door == DOOR_CODE )
							state = inputDoorCodeSuccess;
						else
							state = inputDoorCodeFail;
						isKeypadBusy = 0;
						break;
				case inputDoorCodeSuccess:
						state = inputDoorCodeWait;
						break;
				case inputDoorCodeFail:
						state = inputDoorCodeWait;
						break;
                default:
                        state = inputDoorCodeWait;
                        break;
        }
        
        switch( state ) {
                case inputDoorCodeWait:
						if( ALARM_MODE == OFF )
							doorData = receivedData;
						else
							doorData = '\0';
                        break;
                case inputDoorCodeRead:
                        if( doorData != '\0' && doorData != 'A' && doorData != 'B' && doorData != 'C' && doorData != 'D' && doorData != '*' && doorData != '#' ) {
                                if( i < 4 ) {
									array[i] = doorData;
									i++;
								}
								isKeypadBusy = 1;
                        }
						else if( doorData == '*' )  {
							int j;
							for( j = 0; j < 4; j++ )
								array[j] = '_';
							i = 0;
						}
						if( doorData == '#' )
							receivedData = '#';
						else
							receivedData = '\0';
                        break;
                case inputDoorCodeDecode:
                        input_door = atoi(array);
                        if( input_door == DOOR_CODE ) {
                                //LCD_ClearScreen();
								if( DOOR == LOCKED )
									DOOR = UNLOCKED;								
								else if( DOOR == UNLOCKED )
									DOOR = LOCKED;				
						}        					
                        {
                                //LCD_ClearScreen();
                                int j;
                                for( j = 0; j < 4; j++ )
                                array[j] = '_';
                        }
                        i = 0;
                        break;
				case inputDoorCodeSuccess:
						LCD_ClearScreen();
						if( DOOR == LOCKED )
							LCD_DisplayString(2,"Door Locking!");
						else
							LCD_DisplayString(1,"Door Unlocking!");
						receivedData = '\0';
						numPhases_door = (180/5.625)*64;
						break;
				case inputDoorCodeFail:
						LCD_ClearScreen();
						LCD_DisplayString(2,"Invalid Code!");
						receivedData = '\0';
						break;
                default:
                        break;
        }
        
        return state;
}

//3
enum displayDoorCodeStates{displayDoorCodeWait,displayDoorCodeDisplay};
int TickFct_displayDoorCode( int state ) {
	switch( state ) {
		case -1:
			state = displayDoorCodeWait;
			break;
		case displayDoorCodeWait:
			if( isKeypadBusy && ALARM_MODE == OFF )
				state = displayDoorCodeDisplay;
			else
				state = displayDoorCodeWait;
			break;
		case displayDoorCodeDisplay:
			if( isKeypadBusy )
				state = displayDoorCodeDisplay;
			else
				state = displayDoorCodeWait;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case displayDoorCodeWait:
			break;
		case displayDoorCodeDisplay:
			//LCD_ClearScreen();
			LCD_Cursor(17);
			LCD_WriteData(array[0]);
			LCD_Cursor(18);
			LCD_WriteData(array[1]);
			LCD_Cursor(19);
			LCD_WriteData(array[2]);
			LCD_Cursor(20);
			LCD_WriteData(array[3]);
			break;
		default:
			break;
	}

	return state;	
}

//4
enum motorStates { A, AB, B, BC, C, CD, D, DA };
int TickFct_motorDoor( int state ) {
	        switch( state ) {
		        case -1:
		        state = A;
		        break;
		        case A:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = AB;
						PORTC = SetBit(PORTC,3,1);
						numPhases_door--;
					}
					else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
							state = DA;
							PORTC = SetBit(PORTC,3,1);
							numPhases_door--;
					}
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case AB:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = B;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = A;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case B:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = BC;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = AB;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case BC:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = C;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = B;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case C:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = CD;
						numPhases_door--;
					}
                        else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                                state = BC;
                                numPhases_door--;
                        }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case CD:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = D;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = C;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case D:
		        if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = DA;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = CD;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        case DA:
					if( DOOR == UNLOCKED && numPhases_door > 0 ) {
						state = A;
						numPhases_door--;
					}
                    else if( (DOOR == LOCKED) && (numPhases_door > 0) ) {
                            state = D;
                            numPhases_door--;
                    }
					else {
						state = A;
						numPhases_door = 0;
						PORTC = SetBit(PORTC,3,0);
					}
					break;
		        default:
					state = A;
		        break;
	        }
	        
	        switch( state ) {
		        case A:
					PORTA = (PORTA & 0x0F ) | 0x10;
					break;
		        case AB:
					PORTA = (PORTA & 0x0F ) | 0x30;
					break;
		        case B:
					PORTA = (PORTA & 0x0F ) | 0x20;
					break;
		        case BC:
					PORTA = (PORTA & 0x0F ) | 0x60;
					break;
		        case C:
					PORTA = (PORTA & 0x0F ) | 0x40;
					break;
		        case CD:
					PORTA = (PORTA & 0x0F ) | 0xC0;
					break;
		        case D:
					PORTA = (PORTA & 0x0F ) | 0x80;
					break;
		        case DA:
					PORTA = (PORTA & 0x0F ) | 0x90;
					break;
		        default:
			        break;
	        }
	        
	        return state;	
}

unsigned char joystick = '\0';
//5
enum detectJoystickStates {detectJoystickWait,detectJoystick};
int TickFct_detectJoystick( int state ) {
	switch( state ) {
		case -1:
			state = detectJoystickWait;
			break;
		case detectJoystickWait:
			if( ALARM_MODE == OFF ) {
				if( joystick == 'u' || joystick == 'd' )
					state = detectJoystick;
				else
					state = detectJoystickWait;
			}
			else
				state = detectJoystickWait;
			break;
		case detectJoystick:
			if( ALARM_MODE == OFF ) {
				if( joystick == 'u' || joystick == 'd' )
					state = detectJoystick;
				else
					state = detectJoystickWait;
			}
			else
				state = detectJoystickWait;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case detectJoystickWait:
			joystick = receivedData;
			PORTC = SetBit(PORTC,4,0);
			PORTC = SetBit(PORTC,5,0);
			break;
		case detectJoystick:
			if( joystick == 'u' ) {
				PORTC = SetBit(PORTC,5,1);
				DRAPES = UP;
				if( numPhases_drapes < 360 )
					numPhases_drapes += 50;
			}
			else if( joystick == 'd' ) {
				PORTC = SetBit(PORTC,4,1);
				DRAPES = DOWN;
				if( numPhases_drapes < 360 )
					numPhases_drapes += 50;
			}
			joystick = receivedData;
			break;
		default:
			break;
	}
	
	return state;
}

/*************************************/
/*                                   */
/*        ALARM CLOCK STATES         */
/*                                   */
/*************************************/
unsigned char done = '\0';
//6
enum setAlarmTimeStates {setAlarmTimeWait, setAlarmTimeRead, setAlarmTimeDecode};
int TickFct_setAlarmTime( int state ) {
	static int i = 0;
	switch( state ) {
		case -1:
		state = setAlarmTimeWait;
		break;
		case setAlarmTimeWait:
		if( ALARM_MODE == ON && !isAlarmSet ) {
			if( data == '\0' || data == 'b' || data == 'u' || data == 'd' )
			state = setAlarmTimeWait;
			else if( data == '#' )
			state = setAlarmTimeDecode;
			else
			state = setAlarmTimeRead;
		}
		else //if ALARM_MODE == 0FF
		state = setAlarmTimeWait;
		break;
		case setAlarmTimeRead:
		state = setAlarmTimeWait;
		break;
		case setAlarmTimeDecode:
		state = setAlarmTimeWait;
		break;
		default:
		state = setAlarmTimeWait;
		break;
	}
	
	switch( state ) {
		case setAlarmTimeWait:
		data = receivedData;
		break;
		case setAlarmTimeRead:
		if( data != '\0' && data != 'A' && data != 'B' && data != 'C' && data != 'D' && data != '*' && data != '#' ) {
			if( i < 2 ) { // lhs
			lhs_time_array[i] = data;
			i++;
		}
		else if( i < 4 ) { // rhs
		rhs_time_array[i-2] = data;
		i++;
	}
}
else if( data == '*' ) {
	int j;
	for( j = 0; j < 2; j++ ) {
		lhs_time_array[j] = '0';
		rhs_time_array[j] = '0';
	}
	i = 0;
}
receivedData = '\0';
if( data == '#')
receivedData = '#';
break;
case setAlarmTimeDecode:
array[0] = lhs_time_array[0];
array[1] = lhs_time_array[1];
array[3] = rhs_time_array[0];
array[4] = rhs_time_array[1];
input_time = atoi(array);
{
	int j;
	for( j = 0; j < 4; j++ )
	array[j] = '_';
}
alarmTime = input_time;
isAlarmSet = 1;
mtn = 0; // zero out mtn var
i = 0;
receivedData = '\0';
break;
default:
break;
        }
        
        return state;
}

//7
enum displayAlarmTimeStates {displayAlarmTimeInit,displayAlarmTimeDisplay,displayAlarmTimeConfirm};
int TickFct_displayAlarmTime( int state ) {
        switch( state ) {
                case -1:
                        state = displayAlarmTimeInit;
                        break;
                case displayAlarmTimeInit:
                        if( !isAlarmSet && ALARM_MODE == ON )
                                state = displayAlarmTimeDisplay;
                        else
                                state = displayAlarmTimeInit;
                        break;
                case displayAlarmTimeDisplay:
                        if( !isAlarmSet && ALARM_MODE == ON )
                                state = displayAlarmTimeDisplay;
                        else
                                state = displayAlarmTimeConfirm;
                        break;
                case displayAlarmTimeConfirm:
                        state = displayAlarmTimeInit;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case displayAlarmTimeInit:
                        break;
                case displayAlarmTimeDisplay:
                        LCD_ClearScreen();
                        LCD_DisplayString(1,"Set alarm time:");
                        LCD_Cursor(17);
                        LCD_WriteData(lhs_time_array[0]);
                        LCD_Cursor(18);
                        LCD_WriteData(lhs_time_array[1]);
                        LCD_Cursor(19);
                        LCD_WriteData(':');
                        LCD_Cursor(20);
                        LCD_WriteData(rhs_time_array[0]);
                        LCD_Cursor(21);
                        LCD_WriteData(rhs_time_array[1]);
                        break;
                case displayAlarmTimeConfirm:
                        LCD_ClearScreen();
                        LCD_DisplayString(1,"Alarm time:");
                        LCD_Cursor(17);
                        LCD_WriteData(lhs_time_array[0]);
                        LCD_Cursor(18);
                        LCD_WriteData(lhs_time_array[1]);
                        LCD_Cursor(19);
                        LCD_WriteData(':');
                        LCD_Cursor(20);
                        LCD_WriteData(rhs_time_array[0]);
                        LCD_Cursor(21);
                        LCD_WriteData(rhs_time_array[1]);
                        isAlarmSet = 1;
                        break;
                default:
                        break;
        }
        
        return state;
}

//8
enum triggerAlarmStates {triggerAlarmWait,triggerAlarmRead};
int TickFct_triggerAlarm( int state ){
        switch( state ) {
                case -1:
                        state = triggerAlarmWait;
                        break;
                case triggerAlarmWait:
                        if( ALARM_MODE == ON && isAlarmSet ) {
                                if( !GetBit(PINB,1) )
                                        state = triggerAlarmRead;
                                else
                                        state = triggerAlarmWait;
                        }
                        else
                                state = triggerAlarmWait;
                        break;
                case triggerAlarmRead:
                        state = triggerAlarmWait;
                default:
                        break;
        }
        
        switch( state ) {
                case triggerAlarmWait:
                        break;
                case triggerAlarmRead:
                        ALARM = ON;
						DRAPES = UP;
						numPhases_drapes = (720/5.625)*64;
                        break;
                default:
                        break;
        }
        
        return state;   
}

//9
enum speakerStates {speakerOFF,speakerON1,speakerON2};
int TickFct_speaker( int state ) {
        switch( state ) {
                case -1:
                        state = speakerOFF;
                        break;
                case speakerOFF:
                        if( ALARM_MODE == ON && isAlarmSet && ALARM == ON ) 
                                state = speakerON1;
                        else
                                state = speakerOFF;
                        break;
                case speakerON1:
                        if( ALARM_MODE == ON && isAlarmSet && ALARM == ON )
                                state = speakerON2;
                        else
                                state = speakerOFF;
                        break;
                case speakerON2:
                        if( ALARM_MODE == ON && isAlarmSet && ALARM == ON )
                                state = speakerON1;
                        else
                                state = speakerOFF;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case speakerOFF:
                        break;
                case speakerON1:
                        // do something
                        PORTB = SetBit(PORTB,3,1);
                        break;
                case speakerON2:
                        // do something
                        PORTB = SetBit(PORTB,3,0);
                        break;
                default:
                        break;
        }

        return state;   
}

//10
enum dectectMotionStates {detectMotionInit,detectMotionWait,detectMotionRead};
int TickFct_detectMotion( int state ) {
	static unsigned char count;
	switch( state ) {
		case -1:
			state = detectMotionInit;
			break;
		case detectMotionInit:
			state = detectMotionWait;
			break;
		case detectMotionWait:
			if( ALARM_MODE == ON && isAlarmSet && ALARM == ON  && !mtn ) {
				if( GetBit(PINB,2) ) //if motion is sensed,
					state = detectMotionRead;
				else
					state = detectMotionWait;
			}			
			else
				state = detectMotionInit;
			break;
		case detectMotionRead:
			if( ALARM_MODE == ON && isAlarmSet && ALARM == ON && !mtn ) {
				if( GetBit(PINB,2) ) //if motion is sensed,
					state = detectMotionRead;
				else
					state = detectMotionWait;
			}
			else
				state = detectMotionInit;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case detectMotionInit:
			count = 0;
			break;
		case detectMotionWait:
			break;
		case detectMotionRead:
			count++;
			if( count >= 30 ) {
				mtn = 1;
				count = 0;
			}
			PORTB = SetBit(PORTB,0,mtn);
			break;
		default:
			break;
	}

	return state;	
}

//11
int TickFct_motorDrapes( int state ) {
        switch( state ) {
                case -1:
                        state = A;
                        break;
                case A:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = AB;
								PORTC = SetBit(PORTC,2,1);
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = DA;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }                       
                        break;
                case AB:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = B;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = A;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case B:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = BC;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = AB;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case BC:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = C;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = B;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case C:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = CD;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = BC;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case CD:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = D;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = C;
                                numPhases_drapes--;
                         }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case D:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = DA;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = CD;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                case DA:
                        if( DRAPES == UP && numPhases_drapes > 0 ) {
                                state = A;
                                numPhases_drapes--;
                        }
                        else if( (DRAPES == DOWN) && (numPhases_drapes > 0) ) {
                                state = D;
                                numPhases_drapes--;
                        }
                        else {
                                state = A;
                                numPhases_drapes = 0;
								PORTC = SetBit(PORTC,2,0);
                        }
                        break;
                default:
                        state = A;
                        break;
        }
        
        switch( state ) {
                case A:
                        PORTA = (PORTA & 0xF0 ) | 0x01;
                        break;
                case AB:
                        PORTA = (PORTA & 0xF0 ) | 0x03;
                        break;
                case B:
                        PORTA = (PORTA & 0xF0 ) | 0x02;
                        break;
                case BC:
                        PORTA = (PORTA & 0xF0 ) | 0x06;
                        break;
                case C:
                        PORTA = (PORTA & 0xF0 ) | 0x04;
                        break;
                case CD:
                        PORTA = (PORTA & 0xF0 ) | 0x0C;
                        break;
                case D:
                        PORTA = (PORTA & 0xF0 ) | 0x08;
                        break;
                case DA:
                        PORTA = (PORTA & 0xF0 ) | 0x09;
                        break;
                default:
                        break;
        }
        
        return state;
}

//12
enum inputAlarmCodeStates {inputAlarmCodeWait, inputAlarmCodeRead, inputAlarmCodeDecode, inputAlarmCodeConfirm};
int TickFct_inputAlarmCode( int state ) {
        static int i = 0;
        switch( state )  {
                case -1:
                        state = inputAlarmCodeWait;
                        break;
                case inputAlarmCodeWait:
					if( ALARM_MODE == ON && isAlarmSet && ALARM == ON && mtn ) {
						if( data == '\0' )
						state = inputAlarmCodeWait;
						else if( data == '#' )
							state = inputAlarmCodeDecode;
						else
						state = inputAlarmCodeRead;
					}
					else
					state = inputAlarmCodeWait;
					break;
                case inputAlarmCodeRead:
                    state = inputAlarmCodeWait;
                    break;
                case inputAlarmCodeDecode:
						if( input == ALARM_CODE )
							state = inputAlarmCodeConfirm;
						else
							state = inputAlarmCodeWait;
						break;
				case inputAlarmCodeConfirm:
						state = inputAlarmCodeWait;
						break;
                default:
                        state = inputAlarmCodeWait;
                        break;
        }
        
        switch( state ) {
                case inputAlarmCodeWait:
                        data = receivedData;
                        break;
                case inputAlarmCodeRead:
                        if( data != '\0' && data != 'A' && data != 'B' && data != 'C' && data != 'D' && data != '*' && data != '#' ) {
                                if( i < 4 ) {
									array[i] = data;
									i++;
								}
                        }
						else if( data == '*' )  {
							int j;
							for( j = 0; j < 4; j++ )
								array[j] = '_';
							i = 0;
						}
						if( data == '#' )
							receivedData = '#';
						else
							receivedData = '\0';
                        break;
                case inputAlarmCodeDecode:
                        input = atoi(array);
                        if( input == ALARM_CODE ) {
                                //LCD_ClearScreen();
								receivedData = '\0';
                                ALARM = OFF;
								ALARM_MODE = OFF;
								isAlarmSet = 0;
                        }
                        {
                                //LCD_ClearScreen();
                                int j;
                                for( j = 0; j < 4; j++ )
                                array[j] = '_';
                        }
                        i = 0;
                        break;
				case inputAlarmCodeConfirm:
						LCD_ClearScreen();
						LCD_DisplayString(2,"Good Morning!");
						mtn = 0;
						break;
                default:
                        break;
        }
        
        return state;
}

//13
enum displayAlarmCodeStates {displayAlarmCodeWait,displayAlarmCodeLCD1,displayAlarmCodeLCD2};
int TickFct_displayAlarmCode( int state ) {
        int temp = atoi(array);
        switch( state ) {
                case -1:
                        state = displayAlarmCodeWait;
                        break;
                case displayAlarmCodeWait:
						if( ALARM_MODE == ON && isAlarmSet ) {
							if( ALARM == ON )
							state = displayAlarmCodeLCD1;
							else
							state = displayAlarmCodeWait;
						}
						else
						state = displayAlarmCodeWait;
                        break;
                case displayAlarmCodeLCD1:
						if( ALARM_MODE == ON && isAlarmSet ) {
							if( ALARM == ON )
							state = displayAlarmCodeLCD2;
							else
							state = displayAlarmCodeWait;
						}
						else
						state = displayAlarmCodeWait;
                        break;
                case displayAlarmCodeLCD2:
                        if( ALARM_MODE == ON && isAlarmSet ) {
	                        if( ALARM == ON )
	                        state = displayAlarmCodeLCD1;
	                        else
	                        state = displayAlarmCodeWait;
                        }
                        else
                        state = displayAlarmCodeWait;
                        break;
                default:
                        break;
        }
        
        switch( state ) {
                case displayAlarmCodeWait:
                        break;
                case displayAlarmCodeLCD1:
                        LCD_ClearScreen();
                        LCD_DisplayString(1,"Wake up!");
                        LCD_Cursor(17);
                        LCD_WriteData(array[0]);
                        LCD_Cursor(18);
                        LCD_WriteData(array[1]);
                        LCD_Cursor(19);
                        LCD_WriteData(array[2]);
                        LCD_Cursor(20);
                        LCD_WriteData(array[3]);
                        break;
                case displayAlarmCodeLCD2:
                        LCD_ClearScreen();
                        LCD_DisplayString(9,"Wake up!");
                        LCD_Cursor(17);
                        LCD_WriteData(array[0]);
                        LCD_Cursor(18);
                        LCD_WriteData(array[1]);
                        LCD_Cursor(19);
                        LCD_WriteData(array[2]);
                        LCD_Cursor(20);
                        LCD_WriteData(array[3]);
                        break;
                default:
                        break;
        }
        
        return state;
}

int main(void)
{
        //SLAVE
        SPI_ServantInit();
        DDRA = 0xFF; PORTA = 0x00;              
        DDRC = 0xFF; PORTC = 0x00;
        DDRD = 0xFF; PORTD = 0x00;

        
        tasksNum = 13; // declare number of tasks
        task tsks[13]; // initialize the task array
        tasks = tsks; // set the task array
        
        // define tasks
        unsigned char i=0; // task counter
		
		//home automation
		tasks[i].state = -1;
		tasks[i].period = 100;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_setAlarmMode;
		
		i++;
		tasks[i].state = -1;
		tasks[i].period = 100;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_inputDoorCode;
		
		i++;
		tasks[i].state = -1;
		tasks[i].period = 100;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_displayDoorCode;
		
		i++;
		tasks[i].state = -1;
		tasks[i].period = 3;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_motorDoor;
		
		i++;
		tasks[i].state = -1;
		tasks[i].period = 3;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_detectJoystick;
		
		//alarm clock
		i++;
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_setAlarmTime;

        i++;
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_displayAlarmTime;
        
        i++;
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_triggerAlarm;
        
        i++;
        tasks[i].state = -1;
        tasks[i].period = 5;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_speaker;

        i++;
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_detectMotion;
		
		i++;
		tasks[i].state = -1;
		tasks[i].period = 3;
		tasks[i].elapsedTime = tasks[i].period;
		tasks[i].TickFct = &TickFct_motorDrapes;
		
        i++;
        tasks[i].state = -1;
        tasks[i].period = 100;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_inputAlarmCode;
        
        i++;
        tasks[i].state = -1;
        tasks[i].period = 500;
        tasks[i].elapsedTime = tasks[i].period;
        tasks[i].TickFct = &TickFct_displayAlarmCode;
        
        TimerSet(1);
        TimerOn();
        
        LCD_init();
        //LCD_Cursor(17);
        
        while(1)
        {
                //TODO:: Please write your application code
                //SetBit(PORTA,2,1);
                //LCD_DisplayString(1,"Ptrn: ");
        }
}