/*
  Button
 
 Turns on and off a light emitting diode(LED) connected to digital  
 pin 13, when pressing a pushbutton attached to pin 2. 
 
 
 The circuit:
 * LED attached from pin 13 to ground 
 * pushbutton attached to pin 2 from +5V
 * 10K resistor attached to pin 2 from ground
 
 * Note: on most Arduinos there is already an LED on the board
 attached to pin 13.
 
 
 created 2005
 by DojoDave <http://www.0j0.org>
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Button
 */

// constants won't change. They're used here to 
// set pin numbers:
const int buttonOnePin = 2;     // the number of the pushbutton pin
const int buttonTwoPin = 4;
const int buttonThreePin = 8;
const int buttonFourPin = 12;
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonOneState = 0;         // variable for reading the pushbutton status
int buttonTwoState = 0;
int buttonThreeState = 0;
int buttonFourState = 0;

void setup() {
  Serial.begin(9600);
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      
  // initialize the pushbutton pin as an input:
  pinMode(buttonOnePin, INPUT);     
}

char c;
enum states{WAIT,READ1,READ2,READ3,READ4,SEND};
int state = WAIT;
void loop(){
  switch( state ) {
    case WAIT:
      if( buttonOneState == HIGH )
        state = READ1;
      else if( buttonTwoState == HIGH )
        state = READ2;
      else if( buttonThreeState == HIGH )
        state = READ3;
      else if( buttonFourState == HIGH )
        state = READ4;
      else
        state = WAIT;
      break;
    case READ1:
      if( buttonOneState == HIGH )
        state = READ1;
      else
        state = SEND;
      break;
    case READ2:
      if( buttonTwoState == HIGH )
        state = READ2;
      else
        state = SEND;
      break;
    case READ3:
      if( buttonThreeState == HIGH )
        state = READ3;
      else
        state = SEND;
      break;
    case READ4:
      if( buttonFourState == HIGH )
        state = READ4;
      else
        state = SEND;
      break;
    case SEND:
      state = WAIT;
      break;
    default:
      break;
  }
  
  switch( state ) {
    case WAIT:
      // read the state of the pushbutton value:
      buttonOneState = digitalRead(buttonOnePin);
      buttonTwoState = digitalRead(buttonTwoPin);
      buttonThreeState = digitalRead(buttonThreePin);
      buttonFourState = digitalRead(buttonFourPin);
      digitalWrite(ledPin, LOW);
      break;
    case READ1:
      // read the state of the pushbutton value:
      buttonOneState = digitalRead(buttonOnePin);
      c = 'a';
      break;
    case READ2:
      buttonTwoState = digitalRead(buttonTwoPin);
      c = 'z';
      break;
    case READ3:
      buttonThreeState = digitalRead(buttonThreePin);
      c = 'x';
      break;
    case READ4:
      buttonFourState = digitalRead(buttonFourPin);
      c = 'v';
      break;
    case SEND:
      digitalWrite(ledPin, HIGH);
      Serial.print(c);
      break;
    default:
      break;
  }
}
