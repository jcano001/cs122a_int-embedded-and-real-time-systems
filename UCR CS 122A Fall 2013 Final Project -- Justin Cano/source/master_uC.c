/* Partner(s) Name & E-mail:
Justin Cano (Jcano001@ucr.edu) & Winrich Sy (wsy001@ucr.edu)

* Lab Section: 22

* Assignment: Final Project

* Exercise Description: [optional - include for your own benefit]

*

* I acknowledge all content contained herein, excluding template or example

* code, is my own original work.

*/
#include <avr/io.h>
#include "bit.h"
#include "keypad.h"
#include "scheduler.h"
#include "usart_ATmega1284.h"

// Master code
void SPI_MasterInit(void) {
	// Set DDRB to have MOSI, SCK, and SS as output and MISO as input
	DDRB = 0xBF; PORTB = 0x40;
	// Set SPCR register to enable SPI, enable master, and use SCK frequency
	SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0);
	//   of fosc/16  (pg. 168)
	// Make sure global interrupts are enabled on SREG register (pg. 9)
	SREG =0x80;
}

void SPI_MasterTransmit(unsigned char cData) {
	// set SS low
	PORTB = SetBit(PORTB,4,0);
	// data in SPDR will be transmitted, e.g. SPDR = cData;
	SPDR = cData;
	while(!(SPSR & (1<<SPIF))) { // wait for transmission to complete
		;
	}
	// set SS high
	PORTB = SetBit(PORTB,4,1);

}

void A2D_init() {
	ADCSRA |= (1 << ADEN) | (1 << ADSC) | (1 << ADATE);
	// ADEN: Enables analog-to-digital conversion
	// ADSC: Starts analog-to-digital conversion
	// ADATE: Enables auto-triggering, allowing for constant
	//	    analog to digital conversions.
}
unsigned short Vref;

// Pins on PORTA are used as input for A2D conversion
// The value of pinNum determines the pin on PORTA
//    used for A2D conversion
// Valid values range between 0 and 7, where the value
//    represents the desired pin for A2D conversion
void Set_A2D_Pin(unsigned char pinNum) {
	ADMUX = (pinNum <= 0x07) ? pinNum : ADMUX;
}

// Ensure DDRB is setup as output
void transmit_data(unsigned char data) {
	unsigned char i;
	for (i = 0; i < 8 ; ++i) {
		// Sets SRCLR to 1 allowing data to be set
		// Also clears SRCLK in preparation of sending data
		PORTB = 0x08;
		// set SER = next bit of data to be sent.
		PORTB |= ((data >> i) & 0x01);
		// set SRCLK = 1. Rising edge shifts next bit of data into the shift register
		PORTB |= 0x04;
	}
	// set RCLK = 1. Rising edge copies data from the �Shift� register to the �Storage� register
	PORTB |= 0x02;
	// clears all lines in preparation of a new transmission
	PORTB = 0x00;
}


enum lightStates {ON,OFF};
int light1 = OFF;
int light2 = OFF;
int light3 = OFF;
int light4 = OFF;

unsigned char btn = '\0';
unsigned char data = 0x11;
enum masterStates {waitKeypad,readKeypad,writeKeypad};
int TickFct_Keypad( int state ) {
	static unsigned hold;
	switch( state ) {
		case -1:
			state = waitKeypad;
			break;
		case waitKeypad:
			if( btn == '\0' )
			state = waitKeypad;
			else
			state = readKeypad;
			break;
		case readKeypad:
			if( hold == btn )
			state = readKeypad;
			else
			state = writeKeypad;
			break;
		case writeKeypad:
			state = waitKeypad;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case waitKeypad:
			btn = GetKeypadKey();
			hold = btn;
			//PORTA = SetBit(PORTA,2,0);
			break;
		case readKeypad:
			hold = GetKeypadKey();
			break;
		case writeKeypad:
		//btn = GetKeypadKey();
		switch( btn ) {
				case 'A':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case 'B':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case 'C':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case 'D':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '1':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '2':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '3':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '4':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '5':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '6':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '7':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '8':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '9':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '*':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '0':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				case '#':
					data = btn;
					//PORTA = SetBit(PORTA,2,1);
					break;
				default:
					data = '\0';
					//PORTA = SetBit(PORTA,2,0);
					break;
			}
			SPI_MasterTransmit(data);
			data = '\0';
			break;
		default:
			break;
	}
	
	return state;
}

unsigned char btnData = 'b';
enum btnStates {btnWait,btnRead,btnSend};
int TickFct_btn( int state ) {
	switch( state )	{
		case -1:
			state = btnWait;
			break;
		case btnWait:
			if( !GetBit(PINA,1) )
				state = btnRead;
			else
				state = btnWait;
			break;
		case btnRead:
			if( !GetBit(PINA,1) )
				state = btnRead;
			else
				state = btnSend;
			break;
		case btnSend:
			state = btnWait;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case btnWait:
			break;
		case btnRead:
			break;
		case btnSend:
			SPI_MasterTransmit(btnData);
			break;
		default:
			break;
	}
	
	return state;
}

unsigned char up = 'u';
unsigned char down = 'd';
enum joystickStates {joystickWait,joystickLeft,joystickRight,joystickFinish};
int TickFct_joystick(int state) {
	unsigned int i = 0;
	
	unsigned short Vin = ADC;

	switch(state) {
		case -1:
			state = joystickWait;
			break;
		case joystickWait:
			//Vin = ADC;
			if( Vin < Vref - 112 )
				state = joystickLeft;
			else if ( Vin > Vref + 112 )
				state = joystickRight;
			else
				state = joystickWait;
			break;
		case joystickRight:
			//Vin = ADC;
			if ( Vin > Vref + 112 )
				state = joystickRight;
			else
				state = joystickFinish;
			break;
		case joystickLeft:
			//Vin = ADC;
			if( Vin < Vref - 112 )
				state = joystickLeft;
			else
				state = joystickFinish;
			break;
		case joystickFinish:
			state = joystickWait;
			break;
		default:
			state = joystickWait;
			break;
	}

	switch(state) {
		case joystickWait:
			break;
		case joystickLeft:
			//PORTD = SetBit(PORTD,6,1);
			SPI_MasterTransmit(down);
			break;
		case joystickRight:
			//PORTD = SetBit(PORTD,5,1);
			SPI_MasterTransmit(up);
			break;
		case joystickFinish:
			//PORTD = SetBit(PORTD,6,0);
			//PORTD = SetBit(PORTD,5,0);
			SPI_MasterTransmit('\0');
			break;
		default:
			break;
	}

	return state;
}

unsigned char serialData = '\0';
enum readUSARTStates {readUSARTInit,readUSARTRead,readUSARTDisplay};
int TickFct_readUSART( int state ) {
	switch( state ) {
		case -1:
			state = readUSARTInit;
			break;
		case readUSARTInit:
			state = readUSARTRead;
			break;
		case readUSARTRead:
			if( USART_HasReceived(0) )
				state = readUSARTDisplay;
			else
				state = readUSARTRead;
			break;
		case readUSARTDisplay:
			state = readUSARTInit;
			break;
		default:
			break;
	}

	switch( state ) {
		case readUSARTInit:
			//PORTA = SetBit(PORTA,2,0);
			break;
		case readUSARTRead:
			//PORTA = SetBit(PORTA,2,1);
			//USART_Flush(0);
			break;
		case readUSARTDisplay:
			serialData = USART_Receive(0);
			if( serialData == 'a' ) {//|| serialData == 'z' || serialData == 'x' || serialData == 'v' ) {
				if( light1 == OFF )
					light1 = ON;
				else
					light1 = OFF;
				
				light2 = OFF;
				light3 = OFF;
				light4 = OFF;
			}
			else if( serialData == 'z' ) {
				if( light2 == OFF )
					light2 = ON;
				else
					light2 = OFF;
					
				light1 = OFF;
				light3 = OFF;
				light4 = OFF;
			}
			else if( serialData == 'x' ) {
				if( light3 == OFF )
					light3 = ON;
				else
					light3 = OFF;
				
				light1 = OFF;
				light2 = OFF;
				light4 = OFF;
			}
			else if( serialData == 'v' ) {
				if( light4 == OFF )
					light4 = ON;
				else
					light4 = OFF;
					
				light1 = OFF;
				light2 = OFF;
				light3 = OFF;
			}
			if( light1 == OFF &&
				light2 == OFF &&
				light3 == OFF &&
				light4 == OFF ) {
				PORTA = SetBit(PORTA,2,0);
				transmit_data(0x00);
			}
			else
				PORTA = SetBit(PORTA,2,1);
			USART_Flush(0);
			break;
		default:
			break;	
	}

	return state;	
}

enum light1States {light1Init,light1One,light1Two};
int TickFct_light1( int state ) {
	switch( state )	{
		case -1:
			state = light1Init;
			break;
		case light1Init:
			if( light1 == ON )
				state = light1One;
			else
				state = light1Init;
			break;
		case light1One:
			if( light1 == ON )
				state = light1Two;
			else
				state = light1Init;
			break;
		case light1Two:
			if( light1 == ON )
				state = light1One;
			else
				state = light1Init;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case light1Init:
			break;
		case light1One:
			transmit_data(0x00);
			break;
		case light1Two:
			transmit_data(0xFF);
			break;
		default:
			break;
	}
	
	return state;
}

enum light2States {light2Wait,light2On,light2Off};
int TickFct_light2( int state ) {
	switch( state ) {
		case -1:
			state = light2Wait;
			break;
		case light2Wait:
			if( light2 == ON )
				state = light2On;
			else
				state = light2Wait;
			break;
		case light2On:
			if( light2 == ON )
				state = light2On;
			else
				state = light2Off;
			break;
		case light2Off:
			state = light2Wait;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case light2Wait:
			break;
		case light2On:
			transmit_data(0xFF);
			break;
		case light2Off:
			transmit_data(0x00);
			break;
		default:	
			break;
	}

	return state;	
}

enum light3States {light3Init,light3One,light3Two};
int TickFct_light3( int state ) {
	switch( state ) {
		case -1:
			state = light3Init;
			break;
		case light3Init:
			if( light3 == ON )
				state = light3One;
			else
				state = light3Init;
			break;
		case light3One:
			if( light3 == ON )
				state = light3Two;
			else
				state = light3Init;
			break;
		case light3Two:
			if( light3 == ON )
				state = light3One;
			else
				state = light3Init;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case light3Init:
			break;
		case light3One:
			transmit_data(0xAA);
			break;
		case light3Two:
			transmit_data(0x55);
			break;
		default:
			break;
	}

	return state;
}

unsigned char pattern = 0x01;
enum light4States {light4Init,Left,Right};
unsigned char direction = Left;
int TickFct_light4( int state ) {
	switch( state ) {
		case -1:
			state = light4Init;
			break;
		case light4Init:
			if( light4 == ON )
				state = Left;
			else
				state = light4Init;
			break;
		case Left:
			if( light4 == ON ) {
				if( direction == Left)
					state = Left;
				else if( direction == Right )
					state = Right;
			}
			else
				state = light4Init;
			break;
		case Right:
			if( light4 == ON ) {
				if( direction == Left)
					state = Left;
				else if( direction == Right )
					state = Right;
			}
			else
				state = light4Init;
			break;
		default:
			break;
	}
	
	switch( state ) {
		case light4Init:
			pattern = 0x01;
			direction = Left;
			break;
		case Left:
			transmit_data(pattern);
			if( pattern >= 0x80 )
				direction = Right;
			else
				pattern = pattern << 1;
				
			break;
		case Right:
			transmit_data(pattern);
			if( pattern <= 0x01 )
				direction = Left;
			else
				pattern = pattern >> 1;
			break;
		default:
			break;
	}

	return state;	
}

int main(void)
{
	// MASTER
	DDRC = 0xF0; PORTC = 0x0F;
	DDRA = 0xFC; PORTA = 0x03;
	DDRD = 0xFF; PORTD = 0x00;
	DDRB = 0xFF; PORTD = 0x00;

	
	SPI_MasterInit();
	A2D_init();
	initUSART(0);
	
	tasksNum = 8; // declare number of tasks
	task tsks[8]; // initialize the task array
	tasks = tsks; // set the task array
	
	// define tasks
	unsigned char i=0; // task counter
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_Keypad;

	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_btn;
	
	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_joystick;

	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_readUSART;
	
	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_light1;
	
	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_light2;
		
	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_light3;
		
	i++;
	tasks[i].state = -1;
	tasks[i].period = 100;
	tasks[i].elapsedTime = tasks[i].period;
	tasks[i].TickFct = &TickFct_light4;
			

	TimerSet(100);
	TimerOn();
	
	Vref = ADC;
	while(1)
	{
		//TODO:: Please write your application code
		//SetBit(PORTA,2,1);
	}
}